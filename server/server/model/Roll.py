__author__ = 'tim'

class Roll:
    """Encapsulates a particular roll in the profile graph"""

    def __init__(self, name, directory):
        self.name = name
        self.directory = directory
        self.edge_color = 'grey'
        self.node_color = 'grey'
        self.version = ''
        self.release = ''
        self.xml = ''
        self.root_node = None