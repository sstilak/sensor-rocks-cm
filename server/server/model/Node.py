__author__ = 'tim'

class Node:
    """Encapsulates a particular node in the profile graph"""

    def __init__(self, name, roll):
        self.name = name
        self.version = -1
        self.description = ''
        self.copyright = ''
        self.changelog = ''
        self.packages = []
        self.edify_script_files = []
        self.edify_script = ''
        self.edges_from = []
        self.prereqs = []
        self.roll = roll
        self.post_script = ('', '', False)