__author__ = 'tim'

import iptools
from sqlobject import *


class Device(SQLObject):

    ip = IntCol(default=None)
    roll = StringCol(default=None)
    attributes = PickleCol(default=None)
    included_nodes = PickleCol(default=None)

    def __repr__(self):
        return "<Device('%s', '%s','%s','%s','%s')>" %\
               (self.id, self.ip, self.roll, self.attributes, self.included_nodes)

    def _get_ip(self):
        return iptools.ipv4.long2ip(self._SO_get_ip())

    def _set_ip(self, ip_string):
        self._SO_set_ip(long(iptools.ipv4.ip2long(ip_string)))