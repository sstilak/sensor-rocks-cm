__author__ = 'tim'


class Profile:
    """ Encapsulates the profile graph
    """

    def __init__(self):
        self.nodes = {}
        self.rolls = {}
        self.root_node = None