#!/usr/bin/python
# Tim Telfer 2013

import argparse
from controller import roll_manager, device_manager, package_manager, graph_manager
import sys


def main():
    """
    Parses command line arguments, and calls the required modules
    """

    # Setup argument parser
    parser = argparse.ArgumentParser(
        description="Compiles cyanagenmod and android app packages into an installable rom image")
    subparsers = parser.add_subparsers()

    # Arguments to call package_manager functions
    parser_update_packages = subparsers.add_parser("update-packages",
                                                   help="Downloads and builds roll packages from updated source repositories")
    parser_update_packages.set_defaults(func=update_packages)

    # Arguments to call device_manager functions
    parser_deploy_device = subparsers.add_parser("deploy-device",
                                              help="Adds a new device to the db and compiles+installs its roll zip")
    parser_deploy_device.add_argument("-r", "--roll", help="Compile a particular roll, rather than the whole profile")
    parser_deploy_device.add_argument("-a", "--attributes",
                                     help="A list of attributes (conditionals) to follow when compiling the roll")
    parser_deploy_device.set_defaults(func=deploy_device)

    parser_update_device = subparsers.add_parser("update-device",
                                               help="Updates the rolls and attributes on a deployed device")
    parser_update_device.add_argument("-d", "--device_id", help="The id of the device to remove")
    parser_update_device.add_argument("-r", "--roll", help="Compile a particular roll, rather than the whole profile")
    parser_update_device.add_argument("-a", "--attributes",
                                      help="A list of attributes (conditionals) to follow when compiling the roll")
    parser_update_device.set_defaults(func=update_device)

    parser_remove_device = subparsers.add_parser("remove-device", help="Removes selected device from the database")
    parser_remove_device.add_argument("-d", "--device_id", help="The id of the device to remove")
    parser_remove_device.set_defaults(func=remove_device)

    parser_update_devices = subparsers.add_parser("update-devices", help="Pushes out updated packages to devices")
    parser_update_devices.set_defaults(func=update_devices)

    parser_display_all_devices = subparsers.add_parser("display-all-devices", help="TODO")
    parser_display_all_devices.set_defaults(func=display_all_devices)

    parser_reset_database = subparsers.add_parser("reset-database", help="TODO")
    parser_reset_database.set_defaults(func=reset_database)

    # Arguments to call roll_manager functions
    parser_add_roll = subparsers.add_parser("add-roll", help="Adds a new roll to the profile from a zip archive")
    parser_add_roll.set_defaults(func=add_roll)

    parser_compile_roll = subparsers.add_parser("compile-roll", help="Compiles selected roll to an installable zip")
    parser_compile_roll.add_argument("-r", "--roll", help="Compile a particular roll, rather than the whole profile")
    parser_compile_roll.add_argument("-a", "--attributes",
                                     help="A list of attributes (conditionals) to follow when compiling the roll")
    parser_compile_roll.set_defaults(func=compile_roll)

    parser_update_profile_graph = subparsers.add_parser("update-profile-graph",
                                                        help="Creates an updated graph of the current profile")
    parser_update_profile_graph.add_argument("-r", "--roll", help="Create a subgraph for a requested roll")
    parser_update_profile_graph.add_argument("-a", "--atributes",
                                             help="A list of attributes to follow when creating the graph")
    parser_update_profile_graph.set_defaults(func=update_profile_graph)

    parser_view_profile_graph = subparsers.add_parser("view-profile-graph",
                                                      help="Creates and displays an updated graph of the current profile")
    parser_view_profile_graph.add_argument("-r", "--roll", help="Display a subgraph for a requested roll")
    parser_view_profile_graph.add_argument("-a", "--attributes",
                                           help="A list of attributes to follow when creating the graph")
    #TODO: add option to display full graph as faded when using -r or -a
    parser_view_profile_graph.set_defaults(func=view_profile_graph)

    args = parser.parse_args()
    args.func(args)


#---------------------------#
# PACKAGE MANAGER FUNCTIONS #
#---------------------------#

def update_packages(args):
    package_manager.update_packages()


#--------------------------#
# DEVICE MANAGER FUNCTIONS #
#--------------------------#

def deploy_device(args):

    # Load in any attributes to be applied to the roll compilation
    attributes = {}
    if args.attributes:
        attributes = eval(args.attributes)

    # Load in the roll name if one was given
    roll = None
    if args.roll:
        roll = args.roll

    device_manager.deploy_device(roll, attributes)


def update_device(args):

    # Get device id argument
    if not args.device_id:
        sys.exit("Need device id")
    device_id = args.device_id

    # Load in any attributes to be applied to the roll compilation
    attributes = {}
    if args.attributes:
        attributes = eval(args.attributes)

    # Load in the roll name if one was given
    roll = None
    if args.roll:
        roll = args.roll

    device_manager.update_device(device_id, roll, attributes)


def remove_device(args):

    if not args.device_id:
        sys.exit("Need device id")

    device_id = args.device_id

    device_manager.remove_device(device_id)


def update_devices(args):
    device_manager.update_devices()


def display_all_devices(args):
    device_manager.display_all_devices()


def reset_database(args):
    device_manager.reset_database()


#------------------------#
# ROLL MANAGER FUNCTIONS #
#------------------------#

def add_roll(args):
    roll_manager.add_roll()


def compile_roll(args):

    # Load in any attributes to be applied to the roll compilation
    attributes = {}
    if args.attributes:
        attributes = eval(args.attributes)

    # Load in the roll name if one was given
    roll = None
    if args.roll:
        roll = args.roll

    roll_manager.compile_rom(roll, attributes)


#-------------------------#
# GRAPH MANAGER FUNCTIONS #
#-------------------------#

def update_profile_graph(args):
    graph_manager.update_profile_graph()


def view_profile_graph(args):
    graph_manager.view_profile_graph()


if __name__ == "__main__":
    main()