#!/bin/usr/python
# Tim Telfer 2013

import os
from collections import deque
import shutil
import sys
import subprocess
import win32con
import win32api
import re
import ConfigParser

from model import Profile
import xml_manager

# Hardcoded paths
DIR_MAIN = os.path.join(os.getcwd(), '..')
DIR_OUT = os.path.join(DIR_MAIN, 'out')
DIR_OUT_TEMP = os.path.join(DIR_OUT, 'temp')
DIR_OUT_POST_SCRIPT = os.path.join(DIR_OUT_TEMP, 'system', 'etc', 'init.d')
DIR_ROLLS = xml_manager.DIR_ROLLS
FILE_ROM_SCRIPT = os.path.join(DIR_OUT_TEMP, 'META-INF', 'com', 'google', 'android', 'updater-script')
FILE_ROM_ZIP = os.path.join(DIR_OUT, 'rom.zip')
FILE_OUT_EDIFY = os.path.join(DIR_OUT_TEMP, 'updater-script')
FILE_UPDATE_BINARY = os.path.join(DIR_OUT, 'update-binary')
FILE_ROM_UPDATE_BINARY = os.path.join(DIR_OUT_TEMP, 'META-INF', 'com', 'google', 'android', 'update-binary')
FILE_CONFIG = os.path.join(DIR_MAIN, 'config', 'default.conf')
CONFIG_ATTRIBUTE_SECTION = "Attributes"


def add_roll():
    """Module interface for adding a roll to the profile from a zip archive
    NOTE: Not yet implemented
    """
    #TODO
    pass


def compile_rom(roll_name=None, attributes=None):
    """Module interface for compiling a given roll into an installable zip archive
    If no arguments are given, the root roll will be compiled
    """

    rom_filename = FILE_ROM_ZIP

    # If no roll_name is given, assume user wants the root roll
    if roll_name is None:
        roll_name = 'root'

    # Setup attributes and read in defaults
    default_attributes = get_config_attributes()
    if attributes is None:
        attributes = default_attributes
    else:
        attributes = dict(default_attributes.items() + attributes.items())

    # Load up the profile graph
    profile = xml_manager.load_profile()

    # Ensure given roll exists
    if roll_name is not None and roll_name not in profile.rolls:
        sys.exit("roll name doesn't exist, sorry")

    # Create a deque of nodes that will be compiled in this roll
    device_nodes = nodes_under_roll(profile.rolls[roll_name], attributes)

    # Compile nodes into a rom
    compile_nodes_into_rom(rom_filename, deque(device_nodes), attributes)

    # Return nodes dict, attributes dict and filename
    nodes_dict = {node.name: node.version for node in device_nodes}
    return nodes_dict, attributes, rom_filename


def compile_update_rom(device_id, new_roll_name, new_attributes, curr_roll_name, curr_attributes, curr_nodes):

    rom_filename = os.path.join(DIR_OUT, str(device_id) + ".zip")

    # If no roll_name is given, assume user wants the root roll
    if new_roll_name is None:
        new_roll_name = 'root'

    # read in default attributes
    default_attributes = get_config_attributes()
    if new_attributes is None:
        new_attributes = default_attributes
    else:
        new_attributes = dict(default_attributes.items() + new_attributes.items())

    # Load up the profile graph
    profile = xml_manager.load_profile()

    # Ensure given roll exists
    if new_roll_name is not None and new_roll_name not in profile.rolls:
        sys.exit("roll name doesn't exist, sorry")

    # Get list of nodes that should be on updated device
    updated_device_nodes = nodes_under_roll(profile.rolls[new_roll_name], new_attributes)

    # Generate diff list of nodes
    diff_nodes = generate_diff_nodes(updated_device_nodes, new_attributes, curr_nodes, curr_attributes)
    if len(diff_nodes) == 0:
        nodes_dict = {node.name: node.version for node in updated_device_nodes}
        return nodes_dict, new_attributes, None
    print diff_nodes

    # Compile nodes into rom file
    compile_nodes_into_rom(rom_filename, deque(diff_nodes), new_attributes)

    # Return rom filename, updated attributes list, updated included_nodes
    nodes_dict = {node.name: node.version for node in updated_device_nodes}
    return nodes_dict, new_attributes, rom_filename


def nodes_under_roll(roll, attributes):

    nodes_to_explore = [roll.root_node]
    nodes_contained = []

    while len(nodes_to_explore) > 0:
        current_node = nodes_to_explore.pop()
        nodes_contained.append(current_node)

        for child, condition in current_node.edges_from:
            if condition is None:
                nodes_to_explore.append(child)
            if condition in ["%s=='%s'" % (attribute, value) for attribute, value in attributes.items()]:
                nodes_to_explore.append(child)

    return nodes_contained


def extract_nodes_packages(node, directory):

    packages_directory = os.path.join(DIR_ROLLS, node.roll.name, 'packages')

    for package in node.packages:
        subprocess.call(["7z", "x", os.path.join(packages_directory, package + ".zip"), "-o" + directory])


def extract_nodes_scripting(node, attributes):
    """"""

    with open(FILE_OUT_EDIFY, "ab+") as script_out_file:

        for script_file_name in node.edify_script_files:

            with open(os.path.join(DIR_ROLLS, node.roll.name, 'script-files', script_file_name), 'rb') as script_file:
                script_out_file.write(substitute_variables(script_file.read(), attributes))

        script_out_file.write(substitute_variables(node.edify_script, attributes))


def extract_nodes_post_script(node, attributes):
    """"""

    (post_script_name, script, run_once) = node.post_script

    if script == '' or post_script_name == '':
        return

    with open(os.path.join(DIR_OUT_POST_SCRIPT, post_script_name), "wb+") as post_script_file:
        post_script_file.write(substitute_variables(script, attributes))

        if run_once:
            post_script_file.write("\nrm -f /system/etc/init.d/%s;" % post_script_name)

def substitute_variables(text, attributes):

    #for variable_name in re.findall(SUBSTITUTION_VARIABLE_REGEX, text):
    #    print variable_name

    pattern = re.compile(r'@(\w+?)@')

    # Replace all input attribute variables with their values in the text
    for var_name, var_value in attributes.items():
        text = text.replace('@' + var_name + '@', var_value)

    # Check if any variables remain in the text that the attributes didn't cover
    remaining_vars = re.findall(pattern, text)
    if remaining_vars:
        # Missing vars exist, tell the user the list of unique vars required
        sys.exit("Missing attributes required for compile: " + ",".join(set(remaining_vars)))

    return text


def get_config_attributes():

    config_attributes = {}

    # Create a config and fill it from the config file
    config = ConfigParser.ConfigParser()
    config.read(FILE_CONFIG)

    if config.has_section(CONFIG_ATTRIBUTE_SECTION):
        # Get the values from each option and fill the dict
        for attribute, value in config.items(CONFIG_ATTRIBUTE_SECTION):
            config_attributes[attribute] = value

    return config_attributes


def compile_nodes_into_rom(filename, nodes_to_compile, attributes):

    # Prepare/clear the output directories
    if not os.path.isdir(DIR_OUT):
        os.mkdir(DIR_OUT)
    if os.path.isdir(DIR_OUT_TEMP):
        win32api.SetFileAttributes(DIR_OUT_TEMP, win32con.FILE_ATTRIBUTE_NORMAL)
        shutil.rmtree(DIR_OUT_TEMP)
    os.mkdir(DIR_OUT_TEMP)
    os.makedirs(DIR_OUT_POST_SCRIPT)

    while len(nodes_to_compile) > 0:
        current_node = nodes_to_compile.popleft()

        # Ensure node isn't still waiting on its prereqs to be processed
        if set(current_node.prereqs).isdisjoint(nodes_to_compile):
            # Process the node
            extract_nodes_packages(current_node, DIR_OUT_TEMP)
            extract_nodes_scripting(current_node, attributes)
            extract_nodes_post_script(current_node, attributes)
        else:
            # This node isn't ready, add it back to the queue
            nodes_to_compile.append(current_node)

    # Move updater-script to correct location
    if not os.path.exists(os.path.split(FILE_ROM_SCRIPT)[0]):
        os.makedirs(os.path.split(FILE_ROM_SCRIPT)[0])
    shutil.move(FILE_OUT_EDIFY, FILE_ROM_SCRIPT)
    shutil.copy(FILE_UPDATE_BINARY, FILE_ROM_UPDATE_BINARY)

    # Zip up the contents into an installable package
    os.chdir(DIR_OUT_TEMP)
    subprocess.call(['7z', 'a', '-tzip', filename, './'])


def generate_diff_nodes(updated_device_nodes, new_attributes, curr_nodes, curr_attributes):
    """
    updated_device_nodes is a list of node objects
    new_attributes and curr_attributes are both a dict of name/value
    curr_nodes is a dict of node_name:node_value
    """

    # get diff of attributes
    changed_attributes = []
    for attribute_key, attribute_value in new_attributes.iteritems():
        if attribute_key not in curr_attributes.keys():
            changed_attributes.append(attribute_key)
        elif new_attributes[attribute_key] != curr_attributes[attribute_key]:
            changed_attributes.append(attribute_key)

    # Find nodes that didn't exist before, have newer versions, or reference attributes that have changed
    changed_nodes = []
    for node in updated_device_nodes:
        if node.name not in curr_nodes.keys():
            changed_nodes.append(node)
        elif node.version > curr_nodes[node.name]:
            changed_nodes.append(node)
        elif references_attributes(node, changed_attributes):
            changed_nodes.append(node)

    return changed_nodes


def references_attributes(node, attributes):

    # Check if edify script files contain any of the attributes
    for script_file_name in node.edify_script_files:
        with open(os.path.join(DIR_ROLLS, node.roll.name, 'script-files', script_file_name), 'rb') as script_file:
            script_file_text = script_file.read()
            if any('@' + attribute + '@' in script_file_text for attribute in attributes):
                return True

    # Check if edify script from xml contains any of the attributes
    if any('@' + attribute + '@' in node.edify_script for attribute in attributes):
        return True

    # Check if post-script in xml contains any of the attributes
    if any('@' + attribute + '@' in node.post_script for attribute in attributes):
        return True

    # If it gets to here, none of the listed attributes are mentioned
    return False