import os
import sys
from xml.etree import ElementTree
from model import Profile, Node, Roll
import roll_manager

DIR_ROLLS = os.path.join(os.getcwd(), '..', 'rolls')


def load_profile():

    profile = Profile()

    # Load in the rolls, and add them to the profile object
    rolls = load_rolls()
    for roll in rolls:
        profile.rolls[roll.name] = roll

    # Load in the nodes contained within each roll and add them to the profile object
    nodes = load_nodes(rolls)
    for node in nodes:
        profile.nodes[node.name] = node

    # Load in the graph properties from all of the rolls' graph xml files
    load_graphs(rolls, profile)

    # Set the root node
    if not 'root' in profile.nodes:
        sys.exit("Error: root node doesn't exist")
    else:
        profile.root_node = profile.nodes['root']

    return profile


def load_rolls():
    """Loads in all the rolls for the profile based on the 'rolls' subdirectories and the roll xml file"""

    rolls = []

    # Get the list of roll directories
    roll_directories = get_immediate_subdirectories(DIR_ROLLS)

    # Iterate over each roll_directory, creating a roll name for each
    for roll_directory in roll_directories:

        # Create and fill the roll object represented by this directory/xml
        new_roll = load_roll_from_xml(roll_directory)

        # Add roll to the roll list
        rolls.append(new_roll)

    return rolls


def load_nodes(rolls):
    """Loads in all the nodes for the profile based on the contents of each rolls 'nodes' subdirectory"""

    nodes = []

    # Iterate through each roll and load in their contained nodes
    for roll in rolls:

        # Get the name of the roll's nodes subdirectory
        roll_nodes_dir = os.path.join(roll.directory, 'nodes')

        # Ensure the nodes subdirectory exists
        if not os.path.isdir(roll_nodes_dir):
            sys.exit("Error: 'nodes' subdirectory doesn't exist under %s" % roll.directory)

        # Iterate over each of the node xmls and load in that node
        for node_xml in get_immediate_xml_files(roll_nodes_dir):

            # Load in the current node from its xml file
            new_node = load_node_from_xml(node_xml, roll)

            # Check if this node is the roll's root node
            if new_node.name == roll.name:
                roll.root_node = new_node

            # Add node to the nodes list
            nodes.append(new_node)

        # Ensure the roll has a root node
        if roll.root_node is None:
            sys.exit("Error: roll '%s' doesn't have a node of the same name" % roll.name)

    return nodes


def load_graphs(rolls, profile):
    """Loads in the graph structure for the profile based on the contents of each rolls 'graphs' subdirectory"""

    for roll in rolls:

        # Get the directory for the roll's graphs
        roll_graphs_dir = os.path.join(roll.directory, 'graphs', 'default')

        # Ensure that directory exists
        if not os.path.isdir(roll_graphs_dir):
            sys.exit("Error: 'graphs/default/' subdirectory doesn't exist under %s" % roll.directory)

        # Iterate over each of the contained graph xmls and read them in
        for graph_xml in get_immediate_xml_files(roll_graphs_dir):
            parse_graph_xml(graph_xml, profile)


def load_roll_from_xml(roll_directory):

    # Create roll object with directory name as its name
    roll_name = os.path.split(roll_directory)[1]
    roll = Roll(roll_name, roll_directory)

    # Check if roll's xml file exists
    roll.xml = os.path.join(roll.directory, roll.name + ".xml")
    if not os.path.isfile(roll.xml):
        sys.exit("Error: roll directory doesn't contain roll xml of the same name:\n  %s doesn't exist" % roll.xml)

    # Prepare the xml helper objects for parsing
    xml_tree = ElementTree.parse(roll.xml)
    xml_root = xml_tree.getroot()

    # Ensure roll xml root element is valid
    if xml_root.tag != 'roll':
        sys.exit("Error: %s doesn't contain 'roll' as its root tag" % roll.xml)

    # If a name attribute is given, ensure it matches the rolls name (based on directory/xml name)
    if 'name' in xml_root.attrib and xml_root.attrib['name'] != roll.name:
        sys.exit("Error: name attribute in xml root tag ('%s' in %s) doesn't match roll name ('%s')" %
                 (xml_root.attrib['name'], roll.xml, roll.name))

    # Iterate over the xml elements to fill in the roll's attributes
    for child in xml_root:
        if child.tag == 'color':
            if 'edge' in child.attrib:
                roll.edge_color = child.attrib['edge']
            if 'node' in child.attrib:
                roll.node_color = child.attrib['node']
        elif child.tag == 'info':
            if 'version' in child.attrib:
                roll.version = child.attrib['version']
            if 'release' in child.attrib:
                roll.release = child.attrib['release']

    return roll


def load_node_from_xml(node_xml, roll):

    # Create the node object with the filename as its name (excluding extension)
    node_name = os.path.splitext(os.path.split(node_xml)[1])[0]
    node = Node(node_name, roll)

    # Prepare the xml helper objects for parsing
    xml_tree = ElementTree.parse(node_xml)
    xml_root = xml_tree.getroot()

    # Ensure node xml root element is valid
    if xml_root.tag != 'android':
        sys.exit("Error: xml root tag is not 'android' in %s" % node_xml)

    # Run over the xml elements and load them as attributes for the node object
    for child in xml_root:
        if child.tag == 'description':
            node.description = child.text
        elif child.tag == 'version':
            node.version = child.text
        elif child.tag == 'copyright':
            node.copyright = child.text
        elif child.tag == 'changelog':
            node.changelog = child.text
        elif child.tag == 'package':
            if child.text is not None:
                node.packages.append(child.text)
        elif child.tag == 'script-file':
            if child.text is not None:
                if not ('type' in child.attrib):
                    sys.exit("Error: type attribute missing for 'script-file' tag in %s" % node_xml)
                if child.attrib['type'] == 'edify':
                    node.edify_script_files.append(child.text)
        elif child.tag == 'script':
            if child.text is not None:
                if not ('type' in child.attrib):
                    sys.exit("Error: type attribute missing for 'script' tag in %s" % node_xml)
                if child.attrib['type'] == 'edify':
                    node.edify_script += child.text
        elif child.tag == 'post':
            if child.text is not None:
                script = child.text
                if 'name' in child.attrib:
                    post_script_name = child.attrib['name']
                else:
                    post_script_name = node.name
                if 'run-once' in child.attrib and child.attrib['run-once'] == "1":
                    node.post_script = (post_script_name, script, True)
                else:
                    node.post_script = (post_script_name, script, False)

    return node


def parse_graph_xml(file_path, profile):
    """ """

    # Prepare the xml helper objects for parsing
    xml_tree = ElementTree.parse(file_path)
    xml_root = xml_tree.getroot()

    # Ensure graph's xml root element is valid
    if xml_root.tag != 'graph':
        sys.exit("Error: %s doesn't contain 'graph' as its root tag" % file_path)

    # Iterate over the xml elements to read in the edge and order properties
    for child in xml_root:

        # If it's an edge tag, load in the edge
        if child.tag == 'edge':

            # Get the from-node's name
            if 'from' not in child.attrib:
                sys.exit("Error: %s has an 'edge' tag without a 'from' attribute" % file_path)
            from_node_name = child.attrib['from']

            # Get the from-node from it's name
            if not (from_node_name in profile.nodes):
                sys.exit("Error: %s has an 'edge' tag with a non-existent 'from' node ('%s')" % (
                    file_path, from_node_name))
            from_node = profile.nodes[from_node_name]

            # Load in any conditions
            if 'cond' in child.attrib:
                condition = child.attrib['cond']
            else:
                condition = None

            # Retrieve the to-nodes for this edge
            for sub_child in child:

                if sub_child.tag == 'to':
                    # Get the to-node
                    to_nodes_name = sub_child.text
                    if to_nodes_name not in profile.nodes:
                        sys.exit("Error: %s has an 'edge' tag with a non-existent 'to' node ('%s')" % (
                            file_path, to_nodes_name))
                    to_node = profile.nodes[to_nodes_name]

                    # Add this edge to the from-node
                    from_node.edges_from.append((to_node, condition))

        # If it's an order tag, load in the ordering properties
        elif child.tag == 'order':

            # Get the order properties head node name
            if not ('head' in child.attrib):
                sys.exit("Error: %s has an 'order' tag without a 'head' attribute" % file_path)
            head_node_name = child.attrib['head']

            # Get the order properties head node
            if head_node_name not in profile.nodes:
                sys.exit("Error: %s has an 'order' tag with a non-existent 'head' node ('%s')" % (
                    file_path, head_node_name))
            head_node = profile.nodes[head_node_name]

            # Retrieve the tail-nodes for this order property
            for sub_child in child:

                if sub_child.tag == 'tail':
                    # Get the tail node
                    tail_node_name = sub_child.text
                    if not (tail_node_name in profile.nodes):
                        sys.exit("Error: %s has an 'order' tag with a non-existent 'tail' node ('%s')" % (
                            file_path, sub_child.text))
                    tail_node = profile.nodes[tail_node_name]

                    # Add in the order property to the tail-node
                    tail_node.prereqs.append(head_node)


def get_immediate_subdirectories(directory):
    return [os.path.join(directory, name) for name in os.listdir(directory)
            if os.path.isdir(os.path.join(directory, name))]


def get_immediate_files(dir):
    return [os.path.join(dir, name) for name in os.listdir(dir)
            if os.path.isfile(os.path.join(dir, name))]


def get_immediate_xml_files(dir):
    return [name for name in get_immediate_files(dir)
            if os.path.splitext(name)[1] == '.xml']