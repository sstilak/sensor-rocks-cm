import os
import subprocess
import xml_manager

FILE_GRAPH = os.path.join(os.getcwd(), '..', 'graph-output/graph.out')
FILE_GRAPH_PS = os.path.join(os.getcwd(), '..', 'graph-output/graph.ps')

def update_profile_graph():
    """Module interface for producing a graphviz ps file of the profile graph"""

    # Read in the profile from the xmls files
    profile = xml_manager.load_profile()

    # Initialize graph
    f = open(FILE_GRAPH, 'wb+')
    f.write("digraph rolls\n")
    f.write("{\n")
    f.write("\tsize=\"100,100\";\n")
    f.write("\trankdir=LR;\n")

    # Output rolls key
    f.write("\tsubgraph clusterkey\n")
    f.write("\t{\n")
    f.write("\t\tlabel=\"Rolls\";\n")
    f.write("\t\tfontsize=32;\n")
    f.write("\t\tcolor=black;\n")

    # Include each roll in the rolls key
    for roll_name, roll in profile.rolls.items():
        f.write("\t\t\"roll-%s\" [style=filled shape=box label=\"%s\" fillcolor=%s];\n" % (
            roll.name, roll.name, roll.node_color))

    # Close the rolls key section
    f.write("\t}\n")

    # Output ordering
    f.write("\tsubgraph clusterorder\n")
    f.write("\t{\n")
    f.write("\t\tlabel=\"Ordering Constraints\";\n")
    f.write("\t\tfontsize=32;\n")
    f.write("\t\tcolor=black;\n")

    #TODO: Make this more efficient
    nodes_in_ordering_graph = []
    for node_name, node in profile.nodes.items():
        if node.prereqs:
            if not node in nodes_in_ordering_graph:
                nodes_in_ordering_graph.append(node)
            for prereq_node in node.prereqs:
                if not prereq_node in nodes_in_ordering_graph:
                    nodes_in_ordering_graph.append(prereq_node)
    for node in nodes_in_ordering_graph:
        f.write("\t\t\"order-%s\" [style=filled shape=ellipse label=\"%s\" fillcolor=%s color=black];\n" % (
            node.name, node.name, node.roll.node_color))
    for node in nodes_in_ordering_graph:
        for prereq_node in node.prereqs:
            f.write("\t\t\"order-%s\" -> \"order-%s\" [style=bold color=%s arrowsize=1.5];\n" % (
                prereq_node.name, node.name, node.roll.edge_color))

    # Close the ordering section
    f.write("\t}\n")

    # Output profile graph
    f.write("\tsubgraph clustermain\n")
    f.write("\t{\n")
    f.write("\t\tlabel=\"Profile Graph\";\n")
    f.write("\t\tfontsize=32;\n")
    f.write("\t\tcolor=black;\n")
    f.write("\n")

    # Write in each node
    for node_name, node in profile.nodes.items():
        if node == node.roll.root_node:
            node_shape = 'box'
        else:
            node_shape = 'ellipse'
        f.write("\t\t\"%s\" [style=filled shape=%s label=\"%s\" fillcolor=%s color=black];\n" % (
            node_name, node_shape, node_name, node.roll.node_color))
    f.write("\n")

    # Write in all of the edges
    root_node = profile.nodes['root']
    f.write(get_graphviz_edges(root_node, []))

    # Close the profile graph section
    f.write("\t}\n")

    # Close the graph file
    f.write("}\n")
    f.close()

    # Convert graph.out to graph.ps
    subprocess.call(["dot", "-Tps", FILE_GRAPH, "-o", FILE_GRAPH_PS])

    print "\nGraph image successfully created, and is located at:\n %s\n" % FILE_GRAPH_PS


def view_profile_graph():
    """Module interface for producing a graphviz ps file of the profile graph and displaying it with evince"""
    # TODO: Accept a roll as an argument (plus conditions)

    update_profile_graph()
    print "Opening graph.ps\n"
    subprocess.call(["evince", FILE_GRAPH_PS])


def get_graphviz_edges(node, visited):

    # Ensure the given node has not been visited
    if node in visited:
        return ''

    # Set the current node as visited
    visited.append(node)

    edge_string = ''

    # Find each edge coming from this node
    for (child, condition) in node.edges_from:
        if condition is None:
            edge_string += "\t\t\"%s\" -> \"%s\" [style=bold color=%s  arrowsize=1.5];\n%s" % (
                node.name, child.name, node.roll.edge_color, get_graphviz_edges(child, visited))
        else:
            condition_node_label = node.name + "_" + child.name + "_" + condition
            edge_string += "\t\t\"%s\" [shape=none label=\"%s\"];\n" % (condition_node_label, condition)
            edge_string += "\t\t\"%s\" -> \"%s\" [style=bold color=%s arrowsize=0];\n" % (
                node.name, condition_node_label, node.roll.edge_color)
            edge_string += "\t\t\"%s\" -> \"%s\" [style=bold color=%s arrowsize=1.5];\n" % (
                condition_node_label, child.name, node.roll.edge_color)

    return edge_string