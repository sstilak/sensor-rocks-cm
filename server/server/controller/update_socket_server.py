import socket
import os
import thread


def update_devices(to_update):

    print "Starting update server. Be sure to wait until all updates are completed"

    s = socket.socket()
    s.bind(("192.168.0.3", 5001))
    s.listen(10)  # Queues up to 10 connections before refusing

    while len(to_update) > 0:

        client_socket, address = s.accept()
        (client_ip, client_port) = address
        print "Socket connected with address: " + str(address)

        query = client_socket.recv(4096).strip()
        print "Client queried: " + query

        if query == "Updates?":
            if client_ip in to_update.keys():
                # Get the size of the file to send
                update_filename = to_update[client_ip]
                update_size = os.stat(update_filename).st_size

                # Send message with the size of the file
                client_socket.sendall(str(update_size) + '\n')

                print "Server responded: " + str(update_size)
            else:
                # Reply that there are no updates
                client_socket.sendall("No\n")

                print "Server responded: no"

            print "Closing connection"
            client_socket.close()

        elif query == "Send Update":
            thread.start_new_thread(send_update, (client_socket, to_update[client_ip]))

            print "Started thread for update to be sent"
        elif query == "Received":
            # Remove ip from devices to update
            del to_update[client_ip]

            print "Closing connection"
            client_socket.close()
        else:
            print "Client query not understood, closing connection"
            client_socket.close()

    print "All updates sent out, closing server"
    s.close()


def send_update(client_socket, filename):

    print "Sending update file " + filename

    with open(filename, "rb") as f:
        client_socket.sendall(f.read())

    print "Update Sent (loaded to buffer), closing connection"
    client_socket.close()