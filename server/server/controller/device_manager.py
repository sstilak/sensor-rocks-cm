#!/bin/usr/python
# Tim Telfer 2013

import subprocess
import roll_manager
import update_socket_server
import time
import os
import sys
import iptools
from sqlobject import *
from model import Device

FILE_DEVICE_PUSH = '/sdcard/rom.zip'
FILE_RECOVERY_SCRIPT = os.path.join(roll_manager.DIR_OUT, "openrecoveryscript")
DIR_DEVICE_RECOVERY_SCRIPT = "/cache/recovery"
FILE_DEVICE_DATABASE = os.path.join(roll_manager.DIR_MAIN, 'devices.db')
ATTRIBUTE_IP = 'wifi_ip'
ATTRIBUTE_IP_MIN = 'wifi_min_ip'
ATTRIBUTE_IP_MAX = 'wifi_max_ip'

sqlhub.processConnection = connectionForURI('sqlite:/' + FILE_DEVICE_DATABASE)
if not Device.tableExists():
    Device.createTable()


def deploy_device(roll_name=None, attributes=None):

    # Load in the devices ip address (based on the ips already deployed in the database)
    if attributes and ATTRIBUTE_IP not in attributes:
        attributes[ATTRIBUTE_IP] = generate_next_ip_address()

    # Compile the rom zip
    (compiled_nodes, compiled_attributes, rom_filename) = roll_manager.compile_rom(roll_name, attributes)

    # Push onto device and install
    install_onto_device(rom_filename)

    # Add the device to the deployed devices database
    device = Device(ip=attributes[ATTRIBUTE_IP], roll=roll_name, attributes=attributes,
                    included_nodes=compiled_nodes)

    print "Device %s deployed" % str(device.id)


def edit_device():
    pass


def remove_device(device_id):

    if len(list(Device.select(Device.q.id == device_id))) > 0:
        Device.delete(device_id)
        print "Removed device with id: " + device_id
    else:
        print "Device with id %s does not exist" % device_id


def update_device(device_id, new_roll_name=None, new_attributes=None):

    # Load in the devices IP as an attribute
    device = Device.get(device_id)
    new_attributes[ATTRIBUTE_IP] = device.ip

    # Compile diff rom
    (nodes, attributes, rom_filename) = roll_manager.compile_update_rom(device_id, new_roll_name, new_attributes,
                                                                        device.roll, device.attributes,
                                                                        device.included_nodes)

    if rom_filename is None:
        print "Pushed update not required for the given properties"
    else:
        # Setup socket server to update the device with the new rom
        update_socket_server.update_devices({device.ip: rom_filename})

    # Modify the device in the database
    device.ip = new_attributes[ATTRIBUTE_IP]
    device.roll = new_roll_name
    device.attributes = attributes
    device.included_nodes = nodes

    print "Device update complete"


def update_devices():

    devices_to_update = {}

    # Get all the devices
    all_devices = list(Device.select())

    for device in all_devices:
        # Compile diff rom
        (nodes, attributes, rom_filename) = roll_manager.compile_update_rom(device.id, device.roll, device.attributes,
                                                                            device.roll, device.attributes,
                                                                            device.included_nodes)

        # If an update was produced, add this device to the list of devices to be updated
        if rom_filename is not None:
            devices_to_update[device.ip] = rom_filename

            # Update the devices db properties as well
            device.attributes = attributes
            device.included_nodes = nodes

    print "%s devices to be updated" % str(len(devices_to_update))

    # Setup socket server to update the devices with their new rom
    update_socket_server.update_devices(devices_to_update)

    print "Updating completed"


def reset_database():

    Device.dropTable()


def display_all_devices():

    print list(Device.select())


def generate_next_ip_address():

    # Get max and min IP from conf file
    conf_attributes = roll_manager.get_config_attributes()
    if not ATTRIBUTE_IP_MIN in conf_attributes or not ATTRIBUTE_IP_MAX in conf_attributes:
        sys.exit("Missing default attributes for " + ATTRIBUTE_IP_MAX + " and " + ATTRIBUTE_IP_MIN)
    ip_min = conf_attributes[ATTRIBUTE_IP_MIN]
    ip_max = conf_attributes[ATTRIBUTE_IP_MAX]

    # Get all used ips
    all_used_ips = []
    for device in list(Device.select()):
        all_used_ips.append(device.ip)

    # Find the next ip in the sequence that is not used
    for ip in iptools.IpRange(ip_min, ip_max):
        if ip not in all_used_ips:
            return ip

    # If it gets to here, no ips are available
    sys.exit("No IPs were remaining in the given set")


def install_onto_device(rom_file):

    # Create the openrecoveryscript
    recovery_script_text = "print 'installing rom'\n" \
                           "install /sdcard/rom.zip\n" \
                           "print 'wiping data, cache and dalvik'\n" \
                           "wipe data\n" \
                           "wipe cache\n" \
                           "wipe dalvik"
    with open(FILE_RECOVERY_SCRIPT, "wb+") as recovery_script:
        recovery_script.write(recovery_script_text)

    # Prepare adb (setup as root, so permissions are there
    subprocess.call(["adb", "root"])
    time.sleep(2)   # Necessary as adb root takes an extra second or two after it returns

    # Push the rom zip to the device
    subprocess.call(["adb", "push", rom_file, FILE_DEVICE_PUSH])

    # Push the recovery script to the device
    subprocess.call(["adb", "push", FILE_RECOVERY_SCRIPT, DIR_DEVICE_RECOVERY_SCRIPT])

    # Reboot the device into recovery mode so script will run and rom will install
    subprocess.call(["adb", "reboot", "recovery"])

    # Close the adb server
    subprocess.call(["adb", "kill-server"])

