import socket
import os

filename = 'out/rom.zip'


def start_server():
    s = socket.socket()
    s.bind(("192.168.0.3", 5001))
    s.listen(10)  # Queues up to 10 connections before refusing
    i = 0

    while True:
        #Note: Currently only handles one connection at a time

        sc, address = s.accept()  # Accept an incoming connection
        print "Socket connected with address: " + str(address)

        query = sc.recv(4096).strip()
        print "Clients queried: " + query

        if query == "Updates?":
            print "Client queried for updates"

            if i % 2 == 0:
                print "Telling client there are no updates this time"
                sc.sendall("No\n")
            else:
                update_size = os.stat(filename).st_size
                print "Telling client there is an update of size: " + str(update_size)
                sc.sendall(str(update_size) + '\n')

            i += 1
        elif query == "Send Update":
            print "Client requested update to be sent"

            with open(filename, "rb") as f:
                sc.sendall(f.read())
                print "Update sent"
        else:
            print "Query not understood"

        print "Closing socket"
        sc.close()

    print "Closing server"
    s.close()

if __name__ == "__main__":
    start_server()