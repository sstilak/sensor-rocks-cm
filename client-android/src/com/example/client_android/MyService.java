package com.example.client_android;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {

    private static final String TAG = "MyService";

    private NotificationHandler notificationHandler;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "Service started");

        new ReceiveFileTask(getApplicationContext(), notificationHandler).execute();

        // returning START_STICKY means service will restart if terminated, intent data passed to onStartCommand is null
        return Service.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        Log.d(TAG, "Service created");

        notificationHandler = new NotificationHandler(this, getApplicationContext());
    }

    @Override
    public void onDestroy() {

        Log.d(TAG, "Service destroyed");
    }
}
