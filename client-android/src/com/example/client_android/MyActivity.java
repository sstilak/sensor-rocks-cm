package com.example.client_android;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.*;
import java.net.Socket;
import java.util.Calendar;

/**
 * Main activity, used to display scheduling and unscheduling buttons for testing purposes
 */
public class MyActivity extends Activity {

    private Context context;

    private TextView text;
    private Button scheduleButton;
    private Button unscheduleButton;
    private Button runButton;

    private static final String TAG = "MyActivity";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        Log.d(TAG, "Main Activity created");

        super.onCreate(savedInstanceState);

        context = getApplicationContext();
        final AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        final Intent serviceIntent = new Intent(context, MyService.class);
        final PendingIntent servicePendingIntent =
                PendingIntent.getService(context, 0, serviceIntent, 0);

        setContentView(R.layout.main);
        text = (TextView) findViewById(R.id.text2);
        scheduleButton = (Button) findViewById(R.id.button_schedule);
        unscheduleButton = (Button) findViewById(R.id.button_unschedule);
        runButton = (Button) findViewById(R.id.button_run);

        scheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Schedule service to run every 1 min
                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60*1000, servicePendingIntent);
                Log.d(TAG, "Service scheduled to run every 60 seconds");
            }
        });

        unscheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Cancel scheduled repeating service
                alarmManager.cancel(servicePendingIntent);
                Log.d(TAG, "Service unscheduled. Will not run until rescheduled.");
            }
        });

        runButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Run service intent
                startService(serviceIntent);
            }
        });
    }
}
