package com.example.client_android;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ScheduleServiceAtBootReceiver extends BroadcastReceiver {

    private static final String TAG = "ScheduleServiceAtBootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        Intent serviceIntent = new Intent(context, MyService.class);
        PendingIntent servicePendingIntent = PendingIntent.getService(context, 0, serviceIntent, 0);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60*1000, servicePendingIntent);

        Log.d(TAG, "Service scheduled to run every 60 seconds");
    }
}
