package com.example.client_android;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;

import java.io.*;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Communicates with the server to check for and download updates. If an update is received, it prepares an installation
 * script and reboots the device into recovery mode for that script to run
 */
class ReceiveFileTask extends AsyncTask<Void, Long, Void> {

    private static final String TAG = "ReceiveFileTask";
    private static boolean isRunning = false;
    private static final int SOCKET_BUFFER_SIZE = 65536;

    private Context context;
    private NotificationHandler notificationHandler;
    private File rom;

    private static String server_ip = null;
    private static int server_port = -1;

    /**
     * General constructor
     * @param context The parent service/apps context object, to provide access to strings and other facilities
     */
    public ReceiveFileTask(Context context, NotificationHandler notificationHandler) {
        super();
        this.context = context;
        this.notificationHandler = notificationHandler;
        if (server_ip == null || server_port == -1) {
            setupServerConf();
        }
    }

    /**
     * Performs the main update checking/downloading/installing sequence.
     * @param voids No parameters needed for this
     * @return No return value needed for this
     */
    @Override
    protected Void doInBackground(Void... voids) {

        // Ensure task isn't already running in a separate thread
        if (isRunning) {
            Log.d(TAG, "Task is already running in a separate thread, this one will not run");
            return null;
        }
        isRunning = true;

        try {
            long updateSize = queryServerForUpdate();
            if (updateSize > -1) {
                downloadUpdate(updateSize);
                notifyUpdateReceived();
                createScript();
                rebootToRecovery();
            }
        } catch (Exception e) {
            Log.e(TAG, "Error in update process", e);
        }

        // Note that the task has finished
        isRunning = false;

        return null;
    }

    /**
     * Updates the progress notification
     * @param longs First long is bytes downloaded, second long is total size in bytes
     */
    @Override
    protected void onProgressUpdate(Long... longs) {
        long bytesDownloaded = longs[0];
        long updateSize = longs[1];

        notificationHandler.notifyDownloadProgress(bytesDownloaded, updateSize);

    }

    /**
     * Queries the server to check if an update is available
     * @return The size of the update (in bytes) if one is available, -1 if no update available
     * @throws IOException An error occurred whilst connected to the server
     */
    private long queryServerForUpdate() throws IOException {

        Log.d(TAG, "Attempting to connect to server to query for updates");

        Socket socket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            // Setup socket, reader and writer
            socket = new Socket();
            socket.connect(new InetSocketAddress(server_ip, server_port), 5000); // 5sec timeout
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            // Send query
            out.println(context.getString(R.string.service_update_query));

            // Retrieve answer
            String receivedText = in.readLine();
            if (receivedText == null) {
                Log.e(TAG, "Server did not return a message, assuming no updates");
                throw new IOException("Server returned null response to query");
            }

            // Process answer
            receivedText = receivedText.trim();
            if (receivedText.equals(context.getString(R.string.service_no_updates_response))) {
                Log.d(TAG, "Server queried and there are no updates to be sent");
                return -1;
            } else {
                Log.d(TAG, "Server queried and there is an update of size: " + receivedText);
                return Long.valueOf(receivedText);
            }

        } catch (ConnectException e) {
            Log.d(TAG, "Appears like server is not running. Assuming no updates to be sent: " + e.getMessage());
            return -1;

        } catch (IOException e) {
            Log.e(TAG, "Error querying server, assume no updates to be sent", e);
            throw e;

        } finally {
            // Close streams and socket
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error closing input stream", e);
                }
            }
            if (out != null) {
                out.close();
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error closing socket", e);
                }
            }
        }
    }

    /**
     * Retrieves the update rom from the server
     * @param updateSize The size of the update (for checking/notifying purposes)
     * @throws IOException An error occurs retreiving the update from the server
     */
    private void downloadUpdate(long updateSize) throws IOException {

        Log.d(TAG, "Attempting to connect to server to download update of size: " + updateSize);

        BufferedOutputStream fileOut = null;
        Socket socket = null;
        PrintWriter out = null;
        InputStream in = null;

        int bytesReceived;
        long totalBytesReceived = 0;
        byte[] bytes = new byte[SOCKET_BUFFER_SIZE];

        try {
            // Prepare file and ensure it is writeable
            if (!isExternalStorageWritable()) {
                Log.e(TAG, "Not able to write to external storage.");
                throw new IOException("Cannot write to external storage");
            }
            this.rom = new File(Environment.getExternalStorageDirectory(), context.getString(R.string.rom_file_name));
            Log.d(TAG, "Attempting to store download to: " + rom.getPath());

            // Setup socket and streams
            fileOut = new BufferedOutputStream(new FileOutputStream(rom));
            socket = new Socket();
            socket.connect(new InetSocketAddress(server_ip, server_port), 5000); // 5sec timeout
            out = new PrintWriter(socket.getOutputStream(), true);
            in = socket.getInputStream();

            // Send request to server
            out.println(context.getString(R.string.service_update_request));

            // Receive and store file
            notificationHandler.notifyDownloadProgress(0, updateSize);
            while ((bytesReceived = in.read(bytes)) > 0) {
                fileOut.write(bytes, 0, bytesReceived);

                totalBytesReceived += bytesReceived;
                this.publishProgress(totalBytesReceived, updateSize);
            }

            Log.d(TAG, "Successfully received " + totalBytesReceived + " bytes");

        } catch (IOException e) {
            Log.e(TAG, "Error downloading file from server", e);
            throw e;

        } finally {
            // Close streams and socket
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error closing input stream", e);
                }
            }
            if (out != null) {
                out.close();
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error closing socket", e);
                }
            }
            if (fileOut != null) {
                try {
                    fileOut.flush();
                    fileOut.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error closing file stream", e);
                }
            }
        }
    }

    private void notifyUpdateReceived() throws IOException {
        Log.d(TAG, "Attempting to tell server update was received");

        Socket socket = null;
        PrintWriter out = null;

        try {
            // Setup socket, reader and writer
            socket = new Socket();
            socket.connect(new InetSocketAddress(server_ip, server_port), 5000); // 5sec timeout
            out = new PrintWriter(socket.getOutputStream(), true);

            // Send query
            out.println(context.getString(R.string.service_update_received));

            Log.d(TAG, "Received message successfully sent");

        } catch (IOException e) {
            Log.e(TAG, "Error sending messaged to server", e);
            throw e;

        } finally {
            // Close streams and socket
            if (out != null) {
                out.close();
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error closing socket", e);
                }
            }
        }
    }

    /**
     * Checks if the external storage is mounted with write privileges
     * @return True if able to write to external storage, false otherwise
     */
    private boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /**
     * Creates and fills the openrecoveryscript file
     * @throws FileNotFoundException The openrecoveryscript file could not be opened
     */
    private void createScript() throws IOException {

        Log.d(TAG, "Attempting to create openrecoveryscript");

        PrintWriter scriptWriter = null;
        String scriptText = String.format(context.getString(R.string.openrecoveryscript), rom.getAbsolutePath());

        try {
            // Create the OpenRecoveryScript file/writer
            File script = new File(context.getString(R.string.openrecoveryscript_path));
            scriptWriter = new PrintWriter(new FileWriter(script, false));

            // Load the script text into the file
            scriptWriter.println(scriptText);
            Log.d(TAG, "Wrote the following script text: " + scriptText);

        } catch (IOException e) {
            Log.e(TAG, "Error creating/accessing/writing-to file", e);
            throw e;

        } finally {
            // Close file writer
            if (scriptWriter != null) {
                scriptWriter.close();
            }
        }
    }

    /**
     * Reboots the device into recovery mode, from where the openrecoveryscript will auto-run
     * @throws IOException If an error occurs running reboot command
     */
    private void rebootToRecovery() throws IOException {

        Log.d(TAG, "Attempting to reboot system to recovery mode");

        // Ask PowerManager to reboot the device
        PowerManager powerManager = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
        powerManager.reboot("recovery");
    }

    private void setupServerConf() {
        Log.d(TAG, "Attempting to read in server conf file for ip and port");

        BufferedReader confFileReader = null;

        try {
            File confFile = new File("/system/" + context.getString(R.string.server_conf_file_name));

            confFileReader = new BufferedReader(new FileReader(confFile));

            String conf = confFileReader.readLine();
            String ip = conf.split(";")[0];
            Integer port = Integer.parseInt(conf.split(";")[1]);

            ReceiveFileTask.server_ip = ip;
            ReceiveFileTask.server_port = port;

            Log.d(TAG, "Read in values of ip: " + ReceiveFileTask.server_ip + " port: " + ReceiveFileTask.server_port);

        } catch (IOException e) {
            Log.e(TAG, "Error reading server properties config file", e);
        } finally {
            if (confFileReader != null) {
                try {
                    confFileReader.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error closing file reader", e);
                }
            }
        }
    }
}