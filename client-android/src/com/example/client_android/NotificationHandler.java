package com.example.client_android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.util.Log;

public class NotificationHandler {

    private NotificationManager notificationManager;
    private Service parentService;
    private static final int NOTIFY_ID = 5555017;

    private static final String TAG = "NotificationHandler";

    NotificationHandler(Service service, Context context) {

        ContextWrapper wrapper = new ContextWrapper(context);
        notificationManager = (NotificationManager) wrapper.getSystemService(Context.NOTIFICATION_SERVICE);
        parentService = service;
    }

    public void notifyServiceRunning() {

        setNotification(parentService.getString(R.string.service_running));
    }

    public void notifyServiceStopped() {
        //TODO: Remove notification, or just update it?

        //setNotification(parentService.getText(R.string.service_stopped));
        //cancelNotification();
    }

    public void notifyDownloading() {

        setNotification(parentService.getString(R.string.service_downloading));
    }

    public void notifyFinishedDownload() {

        setNotification(parentService.getString(R.string.service_download_complete));
    }

    public void notifyFailedDownload() {
        setNotification(parentService.getString(R.string.service_download_failed));
    }

    public void notifyDownloadProgress(long bytesReceived, long updateSize) {
        setNotification(String.format(parentService.getString(R.string.service_download_progress), bytesReceived, updateSize));
    }

    public void notifyNoUpdates() {
        setNotification(String.format(parentService.getString(R.string.notify_no_updates)));
    }

    private void setNotification(String message) {

        Log.d(TAG, "Setting notification with message: " + message);

        // The pending intent to launch the activity if notification is pressed
        Intent intent = new Intent(parentService, MyActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(parentService, 0, intent, 0);

        // Create the notification
        Notification notification = new Notification.Builder(parentService)
                .setContentTitle(parentService.getText(R.string.service_name))
                .setContentText(message)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(contentIntent)
                .build();

        // Set the notification
        notificationManager.notify(NOTIFY_ID, notification);
    }

    private void cancelNotification() {

        Log.d(TAG, "Cancelling notification");

        notificationManager.cancel(NOTIFY_ID);
    }
}
