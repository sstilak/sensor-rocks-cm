clear all;
close all;
set(0,'DefaultAxesFontSize', 14);
set(0,'DefaultFigureWindowStyle','docked');
set(0,'DefaultAxesFontName', 'Arial');
set(0,'DefaultAxesFontWeight', 'bold');

maxBI = 300;
numUser = 100;									% will be changed later
R = [25];										% Fixed transmission range
StreetWidth = [2];								% Fixed street width							
BI = [1:maxBI];									% Broadcast interval

linestyles = cellstr(char('-k',':k','-.k','--k','-k',':k','-.k','--k','-k',':k','-k',':k',...
'-.k','--k','-k',':k','-.k','--k','-k',':k','-.k'));

%%%%%%%%%%%%%%%%%%%%  FLOW GENERATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Should I generate two,three, and four flows
usrLoc_free = zeros(length(StreetWidth),numUser);									%%%% Single Flow
usrLoc_opposite = zeros(length(StreetWidth),numUser);								%%%%% THREE FLOWS
usrLoc_cross = zeros(length(StreetWidth),numUser);									%%%%% CROSSING FLOWS


for i=1:length(StreetWidth)

	sigma = 0.25*StreetWidth(i);

	usrLoc_free(i,:) = normrnd(0.5*StreetWidth(i),sigma,[1 numUser]);				% Center, 50%

	while numel(usrLoc_free(i,usrLoc_free(i,:)>StreetWidth(i))) > 0
		usrLoc_free(i,usrLoc_free(i,:)>StreetWidth(i)) = normrnd(0.5*StreetWidth(i),sigma);
	end
	usrLoc_free = abs(usrLoc_free); 

	
	sigma_center = 0.25*StreetWidth(i);
	mu_center = 0.5*StreetWidth(i);
	sigma_side = 0.125*StreetWidth(i);
	mu_side_close = 0.125*StreetWidth(i); 											% Close side
	mu_side_far = 0.875*StreetWidth(i);												% Far side
		
	usrID = 1;
	while usrID > numUser
		if mod(usrID, 3) == 1														% center
			usrLoc_opposite(i,usrID) = normrnd(mu_center,sigma_center);
			
			while usrLoc_opposite(i,usrID) > StreetWidth(i)
				usrLoc_opposite(i,usrID) = normrnd(mu_center,sigma_center);
			end
			
		elseif mod(usrID, 3) == 2													% cloest side
			usrLoc_opposite(i,usrID) = normrnd(mu_side_close,sigma_side);
			
			while usrLoc_opposite(i,usrID) > StreetWidth(i)
				usrLoc_opposite(i,usrID) = normrnd(mu_side_close,sigma_side);
			end
			
		else																		% fartest side
			usrLoc_opposite(i,usrID) = normrnd(mu_side_far,sigma_side);
			
			while usrLoc_opposite(i,usrID) > StreetWidth(i)
				usrLoc_opposite(i,usrID) = normrnd(mu_side_far,sigma_side);
			end
			
		end
		
		usrID = usrID + 1;
	end
	
	usrLoc_opposite = abs(usrLoc_opposite); 
	usrLoc_cross(i,:) = unifrnd(0,StreetWidth(i),[1 numUser]);

end

%%%%%%%%%%%%%%%%%%%% END FLOW GENERATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% usrLoc_free = 25 = zeros(length(StreetWidth),numUser);
% usrLoc_opposite = zeros(length(StreetWidth),numUser);
% usrLoc_cross = zeros(length(StreetWidth),numUser);

Vfree = sort(normrnd(1.49,0.18, [1,numUser]), 'ascend');			% One direction, no congestion
Vopp = sort(normrnd(1.33,0.19, [1,numUser]), 'ascend');				% Oppotsite direction, no congestion
Vcross = sort(normrnd(1.33,0.24, [1,numUser]), 'ascend');			% Crossing, no congestion

Tfree = zeros(length(StreetWidth),numUser);			% Equation (1), free
Topposite = zeros(length(StreetWidth),numUser);		% Equation (1), opposite
Tcross = zeros(length(StreetWidth),numUser);			% Equation (1), cross

for i=1:length(StreetWidth)
	Tfree(i,:) = (2.*sqrt(R(1)^2.- usrLoc_free(i,:).^2))./Vfree;		
	Topposite(i,:) = (2.*sqrt(R(1)^2.- usrLoc_opposite(i,:).^2))./Vopp;		
	Tcross(i,:) = (2.*sqrt(R(1)^2.- usrLoc_cross(i,:).^2))./Vcross;		
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1. Timei and estimated Timei with street width, radius, BIj increasing
%%

Tfree_est = zeros(length(BI),numUser,length(StreetWidth));			% Equation (7), free
Topposite_est = zeros(length(BI),numUser,length(StreetWidth));		% Equation (7), opposite
Tcross_est = zeros(length(BI),numUser,length(StreetWidth));			% Equation (7), cross

Pfree = unifrnd(1,maxBI,1,numUser);								% Parrival, free
Popposite = unifrnd(1,maxBI,1,numUser);							% Parrival, opposite
Pcross = unifrnd(1,maxBI,1,numUser);	 							% Parrival, cross

for i=1:length(StreetWidth)										% Eq. (8)
	for k=1:length(BI)
		
		for j=1:numUser
			if  mod(Tfree(i,j), BI(k)) < (BI(k)- mod(Pfree(j),BI(k)))
				Tfree_est(k,j,i) = floor(Tfree(i,j)/BI(k))*BI(k);
			else
				Tfree_est(k,j,i) = ceil(Tfree(i,j)/BI(k))*BI(k);
			end
			
			if  mod(Topposite(i,j), BI(k)) < (BI(k)- mod(Popposite(j),BI(k)))
				Topposite_est(k,j,i) = floor(Topposite(i,j)/BI(k))*BI(k);
			else
				Topposite_est(k,j,i) = ceil(Topposite(i,j)/BI(k))*BI(k);
			end
			
			if  mod(Tcross(i,j), BI(k)) < (BI(k)- mod(Pcross(j),BI(k)))
				Tcross_est(k,j,i) = floor(Tcross(i,j)/BI(k))*BI(k);
			else
				Tcross_est(k,j,i) = ceil(Tcross(i,j)/BI(k))*BI(k);
			end
			
		end
		
	end
end


figure(1);
w = [5 4 2 1];
BIsel = [1 3 5 10];


for k=1:length(BIsel)
		plot(Tfree_est(BIsel(k),:), linestyles{k}, 'LineWidth',w(k));
		hold on;
end

axis([0 50 20 70]);
h=ylabel('Estimated travel time(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=xlabel('user speed(m/s)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');
h=legend('BI_T_E=1sec', 'BI_T_E=3sec', 'BI_T_E=5sec', 'BI_T_E=10sec');
set(h, 'FontSize',25,'FontWeight', 'bold');


figure(2);
w = [5 4 2 1];
BIsel = [1 3 5 10];


for k=1:length(BIsel)
		plot(Topposite_est(BIsel(k),:), linestyles{k}, 'LineWidth',w(k));
		hold on;
end

axis([0 50 20 70]);
h=ylabel('Estimated travel time(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=xlabel('user speed(m/s)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');
h=legend('BI_T_E=1sec', 'BI_T_E=3sec', 'BI_T_E=5sec', 'BI_T_E=10sec');
set(h, 'FontSize',25,'FontWeight', 'bold');

figure(3);
w = [5 4 2 1];
BIsel = [1 3 5 10];


for k=1:length(BIsel)
		plot(Tcross_est(BIsel(k),:), linestyles{k}, 'LineWidth',w(k));
		hold on;
end

axis([0 50 20 70]);
h=ylabel('Estimated travel time(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=xlabel('user speed(m/s)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');
h=legend('BI_T_E=1sec', 'BI_T_E=3sec', 'BI_T_E=5sec', 'BI_T_E=10sec');
set(h, 'FontSize',25,'FontWeight', 'bold');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2. Estimation error = f(R, vrel, BIj), Eq.(9)
%%

% Tfree = zeros(length(StreetWidth),numUser);			% Equation (1), free
% Topposite = zeros(length(StreetWidth),numUser);		% Equation (1), opposite
% Tcross = zeros(length(StreetWidth),numUser);			% Equation (1), cross

% Tfree_est = zeros(length(BI),numUser,length(StreetWidth));			% Equation (7), free
% Topposite_est = zeros(length(BI),numUser,length(StreetWidth));		% Equation (7), opposite
% Tcross_est = zeros(length(BI),numUser,length(StreetWidth));			% Equation (7), cross

Efree = zeros(length(BI),numUser,length(StreetWidth));			% Error, Equation (9), free
Eopposite = zeros(length(BI),numUser,length(StreetWidth));		% Error, Equation (9), opposite
Ecross = zeros(length(BI),numUser,length(StreetWidth));			% Error, Equation (9), cros

for i=1:length(StreetWidth)										
	for k=1:length(BI)
		for j=1:numUser
			Efree(k,j,i) = abs(Tfree_est(k,j,i) - Tfree(i,j) )/Tfree(i,j);
			Eopposite(k,j,i) = abs(Topposite_est(k,j,i) - Topposite(i,j) )/Topposite(i,j);
			Ecross(k,j,i) = abs(Tcross_est(k,j,i) - Tcross(i,j) )/Tcross(i,j);
		end
	end
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3. Maximum estimation error, 1st term
%%

MaxEfree = zeros(length(BI),numUser,length(StreetWidth));			% Error, Equation (10), free
MaxEopposite = zeros(length(BI),numUser,length(StreetWidth));		% Error, Equation (10), opposite
MaxEcross = zeros(length(BI),numUser,length(StreetWidth));			% Error, Equation (10), cros

for i=1:length(StreetWidth)										
	for k=1:length(BI)
		for j=1:numUser
			MaxEfree(k,j,i) =  BI(k)/Tfree(i,j);
			MaxEopposite(k,j,i) = BI(k)/Topposite(i,j);
			MaxEcross(k,j,i) = BI(k)/Tcross(i,j);
		end
	end
end


figure(4);
w = [5 4 2];

tmp = zeros(3, length(BI));


for i=1:3
	for k=1:length(BI)
		if i==1
			tmp(i,k) = mean(Efree(k,:,1));
		elseif i==2
			tmp(i,k) = mean(Eopposite(k,:,1));
		else
			tmp(i,k) = mean(Ecross(k,:,1));
		end
	end
end

for i=1:3
	plot(BI, tmp(i,:), linestyles{i}, 'LineWidth', w(i));
	hold on;
end

h=xlabel('BI_T_E(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=ylabel('Estimation error') ;
set(h, 'FontSize',25,'FontWeight', 'bold');

h=legend('Uni-direction', 'Opposite-direction', 'Crossing');
set(h, 'FontSize',25,'FontWeight', 'bold');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4. SetTime(nxm) - 3D
% Previous results are SetTimenxm
% Draw plot for users
%%

% This is same as Tfree, Topposite, Tcross

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 5. Deviation = f(BIj)									% Eq. 13
% Tfree = zeros(length(StreetWidth),numUser);			% Equation (1), free
% Topposite = zeros(length(StreetWidth),numUser);		% Equation (1), opposite
% Tcross = zeros(length(StreetWidth),numUser);			% Equation (1), cross

% Tfree_est = zeros(length(BI),numUser,length(StreetWidth));			% Equation (7), free
% Topposite_est = zeros(length(BI),numUser,length(StreetWidth));		% Equation (7), opposite
% Tcross_est = zeros(length(BI),numUser,length(StreetWidth));			% Equation (7), cross

Deviation = zeros(length(BI), 3,length(StreetWidth));										% THREE different FLOWS


for i=1:length(StreetWidth)		
	
	mu_free = mean(Tfree(i,:));
	mu_opposite = mean(Topposite(i,:));
	mu_cross = mean(Topposite(i,:));

	for flw = 1:3
		for k=1:length(BI)
			
			if flw == 1								% FREE
				mu_est = mean(Tfree_est(k,:,i));
				Deviation(k,flw,i) = abs(mu_est-mu_free)/mu_free;
			
			elseif flw == 2							% OPPOSITE
				mu_est = mean(Topposite_est(k,:,i));
				Deviation(k,flw,i) = abs(mu_est-mu_opposite)/mu_opposite;
			else									% CROSSING
				mu_est = mean(Tfree_est(k,:,i));
				Deviation(k,flw,i) = abs(mu_est-mu_cross)/mu_cross;
			end
			
		end
	end
end


%% CALCULATE THE NUMBER OF PACKETS

% Tfree = zeros(length(StreetWidth),numUser);			% Equation (1), free
% Topposite = zeros(length(StreetWidth),numUser);		% Equation (1), opposite
% Tcross = zeros(length(StreetWidth),numUser);			% Equation (1), cross

% Tfree_est = zeros(length(BI),numUser,length(StreetWidth));			% Equation (7), free
% Topposite_est = zeros(length(BI),numUser,length(StreetWidth));		% Equation (7), opposite
% Tcross_est = zeros(length(BI),numUser,length(StreetWidth));			% Equation (7), cross

Nrel_free = zeros(length(BI),numUser,length(StreetWidth));							% # pkts, free
Nrel_opposite = zeros(length(BI),numUser,length(StreetWidth));						% # pkts, opposite
Nrel_cross = zeros(length(BI),numUser,length(StreetWidth));							% # pkts, cross

Nrel_free_est = zeros(length(BI),numUser,4);			% Equation (17), free % Four different BI_T_E : [ 1 3 5 10]
Nrel_opposite_est = zeros(length(BI),numUser,4);		% Equation (17), opposite
Nrel_cross_est = zeros(length(BI),numUser,4);			% Equation (17), cross



for i=1:length(StreetWidth)										
	for k=1:length(BI)
			% real time
			Nrel_free(k,:,i) = floor(Tfree(i,:)./BI(k)); 						% Real #
			Nrel_opposite(k,:,i) = floor(Topposite(i,:)./BI(k));
			Nrel_cross(k,:,i) = floor(Tcross(i,:)./BI(k));	
	end
end

ttf = [1 3 5 10]			% BI_T_E
for i=1:4										
	for k=1:length(BI)		% This BI is BI_a_d_j
		Nrel_free_est(k,:,i) = floor(Tfree_est(ttf(i),:,1)./BI(k)); 				% Eq. 17
		Nrel_opposite_est(k,:,i) = floor(Topposite_est(ttf(i),:,1)./BI(k));
		Nrel_cross_est(k,:,i) = floor(Tcross_est(ttf(i),:,1)./BI(k));
	end
end
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6. Packet reception reliability and overhead of real travel time and estimated travel time 
%%

		%%% PACKET RELIABILITY
Prel_free = zeros(length(BI),length(StreetWidth));							% RELIABILITY, free
Prel_opposite = zeros(length(BI),length(StreetWidth));						% RELIABILITY, opposite
Prel_cross = zeros(length(BI),length(StreetWidth));							% RELIABILITY, cross

Prel_free_est = zeros(length(BI),4);			% Equation (18), free
Prel_opposite_est = zeros(length(BI),4);		% Equation (18), opposite
Prel_cross_est = zeros(length(BI),4);			% Equation (18), cross

		%%% PACKET OVERHEAD
Orel_free = zeros(length(BI),length(StreetWidth));							% RELIABILITY, free
Orel_opposite = zeros(length(BI),length(StreetWidth));						% RELIABILITY, opposite
Orel_cross = zeros(length(BI),length(StreetWidth));							% RELIABILITY, cross

Orel_free_est = zeros(length(BI),4);			% Equation (19), free
Orel_opposite_est = zeros(length(BI),4);		% Equation (19), opposite
Orel_cross_est = zeros(length(BI),4);			% Equation (19), cross

for i=1:length(StreetWidth)										
	for k=1:length(BI)
			
			Prel_free(k,i) =  sum(Nrel_free(k,:,i)>0) / numUser;
			Prel_opposite(k,i) = sum(Nrel_opposite(k,:,i)>0) / numUser;
			Prel_cross(k,i) = sum(Nrel_cross(k,:,i)>0) / numUser;
			
			Orel_free(k,i) =  sum(Nrel_free(k,:,i)) / numUser;
			Orel_opposite(k,i) = sum(Nrel_opposite(k,:,i)) / numUser;
			Orel_cross(k,i) = sum(Nrel_cross(k,:,i)) / numUser;
			
	end
end

for i=1:4			% For 4 different BI_T_E										
	for k=1:length(BI)
		
		Prel_free_est(k,i) = sum(Nrel_free_est(k,:,i)>0) / numUser;
		Prel_opposite_est(k,i) = sum(Nrel_opposite_est(k,:,i)>0) / numUser;
		Prel_cross_est(k,i) = sum(Nrel_cross_est(k,:,i)>0) / numUser;
		
		Orel_free_est(k,i) = sum(Nrel_free_est(k,:,i)) / numUser;
		Orel_opposite_est(k,i) = sum(Nrel_opposite_est(k,:,i)) / numUser;
		Orel_cross_est(k,i) = sum(Nrel_cross_est(k,:,i)) / numUser;
	end
end


figure(8);

w = [ 5 5 3 2 2];

for i=1:5
	if i==1
		tmp = Prel_free.*100;
		plot([1:60], tmp, 'k-o', 'LineWidth', 5);
	else
		tmp = Prel_free_est([1:60],i-1)*100;
		plot([1:60], tmp, linestyles{i}, 'LineWidth', w(i));
	end
	hold on;
end

h=xlabel('BI_a_d_j(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=ylabel('Packet rcv. reliability(%)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');

h=legend('Real', 'BI_T_E=1sec','BI_T_E=3sec', 'BI_T_E=5sec', 'BI_T_E=10sec');
set(h, 'FontSize',20,'FontWeight', 'bold');



figure(9);

w = [ 5 5 3 2 2];

for i=1:5
	if i==1
		tmp = Orel_free;
		plot([1:60], tmp, 'k-o', 'LineWidth', 5);
	else
		tmp = Orel_free_est([1:60],i-1);
		plot([1:60], tmp, linestyles{i}, 'LineWidth', w(i));
	end
	hold on;
end

plot([1:60], ones(1,60), 'k-*', 'LineWidth', 2);

h=xlabel('BI_a_d_j(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=ylabel('Packet rcv. overhead(per user)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');

h=legend('Real', 'BI_T_E=1sec','BI_T_E=3sec', 'BI_T_E=5sec', 'BI_T_E=10sec', 'Optimal');
set(h, 'FontSize',20,'FontWeight', 'bold');



figure(10);

w = [ 5 5 3 2 2];

for i=1:5
	if i==1
		tmp = Prel_opposite.*100;
		plot([1:60], tmp, 'k-o', 'LineWidth', 5);
	else
		tmp = Prel_opposite_est([1:60],i-1)*100;
		plot([1:60], tmp, linestyles{i}, 'LineWidth', w(i));
	end
	hold on;
end

h=xlabel('BI_a_d_j(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=ylabel('Packet rcv. reliability(%)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');

h=legend('Real', 'BI_T_E=1sec','BI_T_E=3sec', 'BI_T_E=5sec', 'BI_T_E=10sec');
set(h, 'FontSize',20,'FontWeight', 'bold');



figure(11);

w = [ 5 5 3 2 2];

for i=1:5
	if i==1
		tmp = Orel_opposite;
		plot([1:60], tmp, 'k-o', 'LineWidth', 5);
	else
		tmp = Orel_opposite_est([1:60],i-1);
		plot([1:60], tmp, linestyles{i}, 'LineWidth', w(i));
	end
	hold on;
end

plot([1:60], ones(1,60), 'k-*', 'LineWidth', 2);

h=xlabel('BI_a_d_j(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=ylabel('Packet rcv. overhead(per user)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');

h=legend('Real', 'BI_T_E=1sec','BI_T_E=3sec', 'BI_T_E=5sec', 'BI_T_E=10sec', 'Optimal');
set(h, 'FontSize',20,'FontWeight', 'bold');


figure(12);

w = [ 5 5 3 2 2];

for i=1:5
	if i==1
		tmp = Prel_cross.*100;
		plot([1:60], tmp, 'k-o', 'LineWidth', 5);
	else
		tmp = Prel_cross_est([1:60],i-1)*100;
		plot([1:60], tmp, linestyles{i}, 'LineWidth', w(i));
	end
	hold on;
end

h=xlabel('BI_a_d_j(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=ylabel('Packet rcv. reliability(%)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');

h=legend('Real', 'BI_T_E=1sec','BI_T_E=3sec', 'BI_T_E=5sec', 'BI_T_E=10sec');
set(h, 'FontSize',20,'FontWeight', 'bold');



figure(13);

w = [ 5 5 3 2 2];

for i=1:5
	if i==1
		tmp = Orel_cross;
		plot([1:60], tmp, 'k-o', 'LineWidth', 5);
	else
		tmp = Orel_cross_est([1:60],i-1);
		plot([1:60], tmp, linestyles{i}, 'LineWidth', w(i));
	end
	hold on;
end

plot([1:60], ones(1,60), 'k-*', 'LineWidth', 2);

h=xlabel('BI_a_d_j(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=ylabel('Packet rcv. overhead(per user)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');

h=legend('Real', 'BI_T_E=1sec','BI_T_E=3sec', 'BI_T_E=5sec', 'BI_T_E=10sec', 'Optimal');
set(h, 'FontSize',20,'FontWeight', 'bold');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9. Determined BIadj based on different restriction
%%

% %%% PACKET RELIABILITY
% Prel_free = zeros(length(BI),length(StreetWidth));							% RELIABILITY, free
% Prel_opposite = zeros(length(BI),length(StreetWidth));						% RELIABILITY, opposite
% Prel_cross = zeros(length(BI),length(StreetWidth));							% RELIABILITY, cross
% Prel_free_est = zeros(length(BI),4);			% Equation (18), free
% Prel_opposite_est = zeros(length(BI),4);		% Equation (18), opposite
% Prel_cross_est = zeros(length(BI),4);			% Equation (18), cross

phi = [0.5:0.05:0.95];

BIadj_free = zeros(1,length(phi));
BIadj_opposite = zeros(1,length(phi));
BIadj_cross = zeros(1,length(phi));

BIadj_free_est = zeros(4,length(phi));
BIadj_opposite_est = zeros(4,length(phi));
BIadj_cross_est = zeros(4,length(phi));

for i=1:length(phi)
	BIadj_free(i) = max(find((Prel_free(:,1)>phi(i))));
	BIadj_opposite(i) = max(find((Prel_opposite(:,1)>phi(i))));
	BIadj_cross(i) = max(find((Prel_cross(:,1)>phi(i))));
end

for k=1:4
	for i=1:length(phi)
		BIadj_free_est(k,i) = max(find((Prel_free_est(:,k)>phi(i))));
		BIadj_opposite_est(k,i) = max(find((Prel_opposite_est(:,k)>phi(i))));
		BIadj_cross_est(k,i) = max(find((Prel_cross_est(:,k)>phi(i))));
	end
end

figure(14);

w = [5 5 3 2 2];

linestyles = cellstr(char('-k',':k','-.k','--k','-k',':k','-.k','--k','-k',':k','-k',':k',...
'-.k','--k','-k',':k','-.k','--k','-k',':k','-.k'));

for i=1:5
	if i==1
		plot([50:5:95], BIadj_free, 'k-o', 'LineWidth', 5);
	else
		plot([50:5:95], BIadj_free_est(i-1,:), linestyles{i}, 'LineWidth', w(i-1));
	end
	hold on;
end

h=xlabel('Reliability threshold=\Psi(%)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');
h=ylabel('BI_a_d_j(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');

h=legend('Real', 'BI_T_E=1sec','BI_T_E=3sec', 'BI_T_E=5sec', 'BI_T_E=10sec');
set(h, 'FontSize',20,'FontWeight', 'bold');


figure(15);

w = [5 5 3 2 2];

linestyles = cellstr(char('-k',':k','-.k','--k','-k',':k','-.k','--k','-k',':k','-k',':k',...
'-.k','--k','-k',':k','-.k','--k','-k',':k','-.k'));

for i=1:5
	if i==1
		plot([50:5:95], BIadj_opposite, 'k-o', 'LineWidth', 5);
	else
		plot([50:5:95], BIadj_opposite_est(i-1,:), linestyles{i}, 'LineWidth', w(i-1));
	end
	hold on;
end

h=xlabel('Reliability threshold=\Psi(%)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');
h=ylabel('BI_a_d_j(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');

h=legend('Real', 'BI_T_E=1sec','BI_T_E=3sec', 'BI_T_E=5sec', 'BI_T_E=10sec');
set(h, 'FontSize',20,'FontWeight', 'bold');

figure(16);

w = [5 5 3 2 2];

linestyles = cellstr(char('-k',':k','-.k','--k','-k',':k','-.k','--k','-k',':k','-k',':k',...
'-.k','--k','-k',':k','-.k','--k','-k',':k','-.k'));

for i=1:5
	if i==1
		plot([50:5:95], BIadj_cross, 'k-o', 'LineWidth', 5);
	else
		plot([50:5:95], BIadj_cross_est(i-1,:), linestyles{i}, 'LineWidth', w(i-1));
	end
	hold on;
end

h=xlabel('Reliability threshold=\Psi(%)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');
h=ylabel('BI_a_d_j(sec)');
set(h, 'FontSize',25,'FontWeight', 'bold');

h=legend('Real', 'BI_T_E=1sec','BI_T_E=3sec', 'BI_T_E=5sec', 'BI_T_E=10sec');
set(h, 'FontSize',20,'FontWeight', 'bold');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 9. Power consumption
%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 6. Length of Traffic estimation slot with BIj and flow level
%%

%%% Until tomorrow during 4 hour

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % 10. Effect of interference of Ki,j
% %%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 11. Effect of transmission range
%%

% Vfree = sort(normrnd(1.49,0.18, [1,numUser]), 'ascend');			% One direction, no congestion
% Vopp = sort(normrnd(1.33,0.19, [1,numUser]), 'ascend');				% Oppotsite direction, no congestion
% Vcross = sort(normrnd(1.33,0.24, [1,numUser]), 'ascend');			% Crossing, no congestion

Rf = [10:1:25];
StreetWidthf = [1 2 3 4 5];

usrLoc_freef = zeros(length(StreetWidthf),numUser);					%%%% Single Flow

for i=1:length(StreetWidthf)
	sigma = 0.25*StreetWidthf(i);
	usrLoc_freef(i,:) = normrnd(0.5*StreetWidthf(i),sigma,[1 numUser]);		% Center, 50%
	while numel(usrLoc_freef(i,usrLoc_freef(i,:)>StreetWidthf(i))) > 0
		usrLoc_freef(i,usrLoc_freef(i,:)>StreetWidthf(i)) = normrnd(0.5*StreetWidthf(i),sigma);
	end
	usrLoc_freef = abs(usrLoc_freef); 
end

usrLoc_oppositef = zeros(length(StreetWidthf),numUser);				%%%%% THREE FLOWS
for i=1:length(StreetWidthf)
	
	sigma_center = 0.25*StreetWidthf(i);
	mu_center = 0.5*StreetWidthf(i);
	
	sigma_side = 0.125*StreetWidthf(i);
	mu_side_close = 0.125*StreetWidthf(i); 			% Close side
	mu_side_far = 0.875*StreetWidthf(i);				% Far side
		
	usrID = 1;
	while usrID > numUser
		if mod(usrID, 3) == 1		% center
			usrLoc_oppositef(i,usrID) = normrnd(mu_center,sigma_center);
			
			while usrLoc_oppositef(i,usrID) > StreetWidthf(i)
				usrLoc_oppositef(i,usrID) = normrnd(mu_center,sigma_center);
			end
			
		elseif mod(usrID, 3) == 2	% cloest side
			usrLoc_oppositef(i,usrID) = normrnd(mu_side_close,sigma_side);
			
			while usrLoc_oppositef(i,usrID) > StreetWidthf(i)
				usrLoc_oppositef(i,usrID) = normrnd(mu_side_close,sigma_side);
			end
			
		else						% fartest side
			usrLoc_oppositef(i,usrID) = normrnd(mu_side_far,sigma_side);
			
			while usrLoc_oppositef(i,usrID) > StreetWidthf(i)
				usrLoc_oppositef(i,usrID) = normrnd(mu_side_far,sigma_side);
			end
			
		end
		
		usrID = usrID + 1;
	end
	
	usrLoc_oppositef = abs(usrLoc_oppositef); 
end

usrLoc_crossf = zeros(length(StreetWidthf),numUser);					%%%%% CROSSING FLOWS
for i=1:length(StreetWidth)
	usrLoc_crossf(i,:) = unifrnd(0,StreetWidthf(i),[1 numUser]);
end

Tfreef = zeros(length(StreetWidthf),numUser,length(Rf));			% Equation (1), free
Toppositef = zeros(length(StreetWidthf),numUser,length(Rf));			% Equation (1), opposite
Tcrossf = zeros(length(StreetWidthf),numUser,length(Rf));			% Equation (1), cross

for k=1:length(Rf)
	for i=1:length(StreetWidthf)
		Tfreef(i,:,k) = (2.*sqrt(Rf(k)^2.- usrLoc_freef(i,:).^2))./Vfree;		
		Toppositef(i,:,k) = (2.*sqrt(Rf(k)^2.- usrLoc_oppositef(i,:).^2))./Vopp;		
		Tcrossf(i,:,k) = (2.*sqrt(Rf(k)^2.- usrLoc_crossf(i,:).^2))./Vcross;		
	end
end

Tfree_estf = zeros(length(StreetWidthf), numUser,length(Rf));			% Equation (7), free
Topposite_estf = zeros(length(StreetWidthf), numUser,length(Rf));		% Equation (7), opposite
Tcross_estf = zeros(length(StreetWidthf), numUser,length(Rf));			% Equation (7), cross

% Pfree = unifrnd(1,60,1,numUser);								% Parrival, free
% Popposite = unifrnd(1,60,1,numUser);							% Parrival, opposite
% Pcross = unifrnd(1,60,1,numUser);	 							% Parrival, cross

for k=1:length(Rf)
	for i=1:length(StreetWidthf)										% Eq. (8)
		for j=1:numUser
			if  mod(Tfreef(i,j,k), BI(1)) < (BI(1)- mod(Pfree(j),BI(1)))
				Tfree_estf(i,j,k) = floor(Tfreef(i,j,k)/BI(1))*BI(1);
			else
				Tfree_estf(i,j,k) = ceil(Tfreef(i,j,k)/BI(1))*BI(1);
			end
			
			if  mod(Toppositef(i,j,k), BI(1)) < (BI(1)- mod(Popposite(j),BI(1)))
				Topposite_estf(i,j,k) = floor(Toppositef(i,j,k)/BI(1))*BI(1);
			else
				Topposite_estf(i,j,k) = ceil(Toppositef(i,j,k)/BI(1))*BI(1);
			end
			
			if  mod(Tcrossf(i,j,k), BI(1)) < (BI(1)- mod(Pcross(j),BI(1)))
				Tcross_estf(i,j,k) = floor(Tcrossf(i,j,k)/BI(1))*BI(1);
			else
				Tcross_estf(i,j,k) = ceil(Tcrossf(i,j,k)/BI(1))*BI(1);
			end
			
		end
	end
end


% Tfreef = zeros(length(StreetWidthf),numUser,length(Rf));			% Equation (1), free
% Toppositef = zeros(length(StreetWidthf),numUser,length(Rf));			% Equation (1), opposite
% Tcrossf = zeros(length(StreetWidthf),numUser,length(Rf));			% Equation (1), cross
% Tfree_estf = zeros(length(Rf),numUser,length(StreetWidthf));			% Equation (7), free
% Topposite_estf = zeros(length(Rf),numUser,length(StreetWidthf));		% Equation (7), opposite
% Tcross_estf = zeros(length(Rf),numUser,length(StreetWidthf));			% Equation (7), cross


rmseFree = zeros(length(StreetWidthf),length(Rf));		% Real Trasit Time % RMSE of Tfreef and Tappx
rmseOpposite = zeros(length(StreetWidthf),length(Rf));		% Real Trasit Time % RMSE of Tfreef and Tappx
rmseCross = zeros(length(StreetWidthf),length(Rf));		% Real Trasit Time % RMSE of Tfreef and Tappx


for i=1:length(StreetWidthf)
	for k=1:length(Rf)
		rmseFree(i,k) = sqrt(sum((Tfreef(i,:,k) - Tfree_estf(i,:,k)).^2)/numUser);		
		rmseOpposite(i,k) = sqrt(sum((Toppositef(i,:,k) - Topposite_estf(i,:,k)).^2)/numUser);		
		rmseCross(i,k) = sqrt(sum((Tcrossf(i,:,k) - Tcross_estf(i,:,k)).^2)/numUser);		
	end
end



% Figure 5. RMSE of real Timet as a function of transmission range and street width
figure(50);
w = [3 3 3 3 7];
for k=1:length(StreetWidthf)
		plot(Rf, rmseFree(k,:), linestyles{k}, 'LineWidth',w(k));
	hold on;
end

h=ylabel('RMSE');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=xlabel('Transmission range(m)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');
h=legend('StreetWidth=1m', 'StreetWidth=2m', 'StreetWidth=3m','StreetWidth=4m','StreetWidth=5m');
set(h, 'FontSize',25,'FontWeight', 'bold');

figure(51);
w = [3 3 3 3 7];
for k=1:length(StreetWidthf)
		plot(Rf, rmseOpposite(k,:), linestyles{k}, 'LineWidth',w(k));
	hold on;
end

h=ylabel('RMSE');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=xlabel('Transmission range(m)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');
h=legend('StreetWidth=1m', 'StreetWidth=2m', 'StreetWidth=3m','StreetWidth=4m','StreetWidth=5m');
set(h, 'FontSize',25,'FontWeight', 'bold');

figure(52);
w = [3 3 3 3 7];
for k=1:length(StreetWidthf)
		plot(Rf, rmseCross(k,:), linestyles{k}, 'LineWidth',w(k));
	hold on;
end

h=ylabel('RMSE');
set(h, 'FontSize',25,'FontWeight', 'bold');
h=xlabel('Transmission range(m)') ;
set(h, 'FontSize',25,'FontWeight', 'bold');
h=legend('StreetWidth=1m', 'StreetWidth=2m', 'StreetWidth=3m','StreetWidth=4m','StreetWidth=5m');
set(h, 'FontSize',25,'FontWeight', 'bold');






