function [out1 out2 out3 out4 out5 out6 out7 out8] = onetimeDelay(buf,bufCnt,currTime, interval, RadioType,bfMax)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% One time delay: 
% Determine optimal delay based on rewared
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check the timeout of each packet


numDelayed = 0;
TotDelay = 0;

% remaining time
x = buf(1:bufCnt-1,2) - currTime + buf(1:bufCnt-1,1);
x = x(x >0);
y = sort(x, 'ascend');

N = max(y); % the largest remaining time can be the time constraint
z = zeros(1, N);
cnty = 1;

for i=1:length(x)-1
    if y(i) == y(i+1)
        z(cnty) = z(cnty) + 1;
    else
        z(cnty) = z(cnty) + 1;
        cnty = cnty + 1;
    end
end

z = z(z~=0);
maxConst = max(x);
Steps = [1:maxConst];
Cnt = length(Steps);

A = zeros(1, Cnt);    % 1st row: state1, 2nd row: state2

% What is the transition probability

t = Cnt;        % t = N
t = t -1;

% Iteration appraoch
% Optimal stopping theory
currR = zeros(1, Cnt);
while t > 1
    
    tmpY = y(y<=t);
    elapseTime = t - tmpY;
    timeConst = tmpY;
    tmp = elapseTime ./ timeConst;
    discount = exp(-tmp);
    currR(t) = sum(discount);
    
    t = t-1;
end

for i=2:Cnt-1
    if currR(i) >= currR(i+1)
        A(i) = 1; % stop
    else
        A(i) = 0; % continue
    end
end


[xx txInstances] = find(A == 1);

if isempty(txInstances) == 1
    nextTxTime = 10;
elseif length(txInstances) >= 2
    nextTxTime = txInstances(1) ; %length(txInstances);
else
    nextTxTime = txInstances(1);
end

numTO = 0;                                  % number of time out pkts
numTX = 0;                                  % number of pkts transmit

for i=1:bufCnt-1
    
    if (buf(i,2) - currTime + buf(i,1)) < nextTxTime
        if (currTime - buf(i,1)) > buf(i,2)          % expired packet
            numTO = numTO + 1;
            buf(i,:) = -1;
        else
            TotDelay = TotDelay + (currTime-buf(i,1));
            numTX = numTX + 1;                  % packet will transmit
            buf(i,:) = -1;   
        end
    end
end

if numTX==0
    conSump = 0;
else
    [conSump xx yy] = energyModel(RadioType, floor(numTX*0.5));
end

% Buffer update: delete expired packets

retBuf = buf(buf(1:bufCnt-1,1) ~= -1, :);

buf2 = zeros(bfMax,3);
ll = size(retBuf);
for i=1:ll(1)
    buf2(i,:) = retBuf(i,:);
end


%BfCas, energyCmp, timeOver, txOver, cntRet, delay

out1 = buf2;
out2 = conSump;
out3 = numTO;
out4 = numTX;
out5 = ll(1)+1;                       % point out next empty buffer
out6 = TotDelay;
out7 = numDelayed;
out8 = nextTxTime;
