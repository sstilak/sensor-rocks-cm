function [out1 out2 out3 out4 out5 out6] = fixedTX(buf,bufCnt,currTime,RadioType,bfMax)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fixed Delay, tx all buffered packets at every sampling interval
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check the timeout of each packet

numTO = 0;                                  % number of time out pkts
numTX = 0;                                  % number of pkts transmit

TotDelay = 0;

for i=1:bufCnt-1
    
    if (currTime - buf(i,1)) > buf(i,2)          % expired packet
        numTO = numTO + 1;
        buf(i,:) = -1;
    else
        TotDelay = TotDelay + (currTime-buf(i,1));
        numTX = numTX + 1;                  % packet will transmit
        buf(i,:) = -1;   
    end
    
end


% Energy consumption
[conSump xx yy] = energyModel(RadioType, numTX);

% Buffer update: delete expired packets
retBuf = buf(buf(1:bufCnt-1,1) ~= -1, :);

buf2 = zeros(bfMax,2);
ll = size(retBuf);
for i=1:ll(1)
    buf2(i,:) = retBuf(i,:);
end

out1 = buf2;
out2 = conSump;
out3 = numTO;
out4 = numTX;
out5 = ll(1)+1;                       % point out next empty buffer
out6 = TotDelay;


