function [out1 out2 out3 out4 out5 out6] = control(buf, bufCnt, currTime, threshold,RadioType, bfMax)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%control.m%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Control Limit: threshold based on own sampling rate, application type
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check the size of the buffer
% Transmit all packets when the size is larger than the threshold

numTO = 0;                                  % number of time out pkts
numTX = 0;                                  % number of pkts transmit
TotDelay = 0;


if bufCnt >= threshold
    numTX = bufCnt;

    % Check timeout packet
    for i=1:bufCnt-1
        if (currTime - buf(i,1)) > buf(i,2)            % expired packet
            numTO = numTO + 1;
            buf(i,:) = -1;                              % mark it expired
        else
            TotDelay = TotDelay + (currTime-buf(i,1));
            numTX = numTX + 1;
        end
    end

    buf2 = zeros(bfMax,2);
    [conSump xx yy] = energyModel(RadioType, numTX);
    bufCnt2 = 1;
    
else
    
    buf2 = buf;
    conSump = 0;
    numTO = 0;
    numTX = 0;
    bufCnt2 = bufCnt;
    TotDelay = 0;
    
end
% Return empty buffer


out1 = buf2;
out2 = conSump;
out3 = numTO;
out4 = numTX;
out5 = bufCnt2;                       % point out next empty buffer
out6 = TotDelay;


