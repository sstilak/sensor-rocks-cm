clear all
close all

set(0, 'DefaultAxesFontSize', 24)
set(0,'DefaultFigureWindowStyle','docked');
set(0, 'DefaultAxesFontName', 'Arial')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ICCCN 2015 simulation platform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Parameters
distToSink = [1:4];
arrivRate = [0.1]; %0.1:0.1:2];            % \lambda = packets per sec

numRate = length(arrivRate);

numRadio = 3;       % the number of radio type, it determines energy consmp
SimTime = 60*60*5; %*60*5; %60*5; %*60;

bfMax = 300;
RadioType = 1;
alpha = 3;


%% Simulation start

% Scenario 4,5,6,7: delay sensitive application 10% 30% 50% 70%

for sc=3:3
    
    scenario=sc;

    switch scenario
        case {1}            % delay sensitive
            tdelta = 3;                            % Sample every 10 sec
            lifetimeSet = [4:1:8]; %tdelta];             % Sec, 30 sec is upper limit
            typesOfApp = length(lifetimeSet);
        case {2}
            tdelta = 10;                            % Sample every 10 sec
            lifetimeSet = [15:1:35]; %tdelta];             % Sec, 30 sec is upper limit
            typesOfApp = length(lifetimeSet);
        case {3}            % Hybrid
            tdelta = 10;                            % Sample every 10 sec
            lifetimeSet = [15:1:35]; %tdelta];             % Sec, 30 sec is upper limit
            typesOfApp = length(lifetimeSet);
        otherwise
            tdelta = 10;                            % Sample every 10 sec
            lifetimeSet = [15:1:35]; %tdelta];             % Sec, 30 sec is upper limit
            typesOfApp = length(lifetimeSet);
    end
    
    decisionInterval = tdelta;  % Ye's paper, 0.1313, I use every 5sec
    mu = 1/decisionInterval;

    
    ResultEnergyConsmpFixed = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufDelayFixed = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufTOFixed = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufTxedFixed = zeros(numRate,typesOfApp,length(distToSink));
    ResultUnitEnergyFixed = zeros(numRate,typesOfApp,length(distToSink)); 


    ResultEnergyConsmpCas = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufDelayCas = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufTOCas = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufTxedCas = zeros(numRate,typesOfApp,length(distToSink));
    ResultUnitEnergyCas = zeros(numRate,typesOfApp,length(distToSink)); 
    
    ResultEnergyConsmpCtrl = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufDelayCtrl = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufTOCtrl = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufTxedCtrl = zeros(numRate,typesOfApp,length(distToSink));
    ResultUnitEnergyCtrl = zeros(numRate,typesOfApp,length(distToSink)); 
    
    
    ResultEnergyConsmpSF = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufDelaySF = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufTOSF = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufTxedSF = zeros(numRate,typesOfApp,length(distToSink));
    ResultUnitEnergySF = zeros(numRate,typesOfApp,length(distToSink)); 
    
    ResultEnergyConsmpOne = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufDelayOne = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufTOOne = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufTxedOne = zeros(numRate,typesOfApp,length(distToSink));
    ResultBufPktsDelayedOne = zeros(numRate,typesOfApp,length(distToSink));
    ResultUnitEnergyOne = zeros(numRate,typesOfApp,length(distToSink)); 
    
    
    
for ld=1:length(arrivRate)

    mainArrivPkts = poissrnd(arrivRate(ld), [SimTime,1]);

    for tA=1:length(lifetimeSet)

        for rd=1:length(distToSink)
            
            numProtocol = 5;
            TotEnergyConsmp = zeros(numProtocol,1);
            bfCnt = ones(numProtocol,1);
            BfFix = zeros(bfMax, 2);               % Col1: gen Time, Col2: lifetime
            BfCas = zeros(bfMax, 2);
            BfCtrl = zeros(bfMax, 2);               % Col1: gen Time, Col2: lifetime
            BfSF = zeros(bfMax, 2);               % Col1: gen Time, Col2: lifetime
            BfOne = zeros(bfMax, 3);               % Col1: gen Time, Col2: lifetime, whether it was delayed or not

            stP= zeros(numProtocol,1);                       % Ptr for current operation time
            stM = 1;                               % timer at main program

            TotGen = zeros(numProtocol,1);                    % Tottal generated packets
            TotLoss = zeros(numProtocol,1);                   % Tottal expired packets
            TotDelay = zeros(numProtocol,1);                  % Total delay of all txed packets
            TotTxed = zeros(numProtocol,1);                   % Total number of packets txed

           %% Fixed
             % Fixed appraoch adjust transmission insterval with DST
             if rd ==1
                stP(1) = tdelta;
             elseif rd == 2
                 stP(1) = tdelta -5;
             elseif rd == 3
                 stP(1) = tdelta + 5;
             else
                 stP(1) = tdelta + 10;
             end

           %% Cascade
            shd = 0.3;
            e = tdelta - (shd*distToSink(rd));
            timeOut = 2*e;
            stP(2) = timeOut;


            %% Control
            lambda = arrivRate(ld);
            alpha = (1/lifetimeSet(tA));
            thrld = ceil( lambda*mu / (alpha*(alpha + mu))) + 1;  
            stP(3) = decisionInterval;

            %% Selected Forwarding
            tau = 1;
            TotDiscard = 0;
            stP(4) = decisionInterval;
            
            %% One-time delay
            stP(5) = 1; %tdelta; %floor(lifetimeSet(tA)/2);
            TotalDelayedPkts = 0;

%%
            while stM < SimTime + 1

                % Those do not consider routing distnace
                if rd==1
                 
                 %% Control-limit
                   if mod(stM, floor(stP(3))) == 0                       

                        % Save generated packets
                        tmpBuffer = mainArrivPkts(stM-floor(stP(3))+1:stM);

                        for i=1:length(tmpBuffer)
                            for j=1:tmpBuffer(i)
                                BfCtrl(bfCnt(3),1) = stM-floor(stP(3)) + i;

%                                if scenario == 
%                                    tmpLF = lifetimeSet(tA);
%                                else
                                
                                if scenario == 3
                                    tmpLF = randi(lifetimeSet(tA));
                                elseif scenario == 4
                                    tmpRnd = binornd(1,0.1);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 5
                                    tmpRnd = binornd(1,0.3);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 6
                                    tmpRnd = binornd(1,0.5);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 7
                                    tmpRnd = binornd(1,0.7);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                else
                                    tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                end

                                BfCtrl(bfCnt(3),2) = tmpLF;
                                bfCnt(3) = bfCnt(3) + 1;
                            end
                        end

                        TotGen(3) = TotGen(3) + sum(tmpBuffer);

                        [BfCtrl, energyCmp, timeOver, txPkts, cntRet, delays] = control(BfCtrl, bfCnt(3), stM,thrld,RadioType,bfMax);
                        TotEnergyConsmp(3) = TotEnergyConsmp(3) + energyCmp;
                        TotLoss(3) = TotLoss(3) + timeOver;
                        TotTxed(3) = TotTxed(3) + txPkts;
                        TotDelay(3) = TotDelay(3) + delays;
                        bfCnt(3) = cntRet;

                    end
                    
                 %%    Selected forwarding
                    if mod(stM, floor(stP(4))) == 0                       

                        % Save generated packets
                        tmpBuffer = mainArrivPkts(stM-floor(stP(4))+1:stM);

                        for i=1:length(tmpBuffer)
                            for j=1:tmpBuffer(i)
                                BfSF(bfCnt(4),1) = stM-floor(stP(4)) + i;

%                                if scenario == 
%                                    tmpLF = lifetimeSet(tA);
%                                else
                                if scenario == 3
                                    tmpLF = randi(lifetimeSet(tA));
                                elseif scenario == 4
                                    tmpRnd = binornd(1,0.1);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 5
                                    tmpRnd = binornd(1,0.3);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 6
                                    tmpRnd = binornd(1,0.5);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 7
                                    tmpRnd = binornd(1,0.7);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                else
                                    tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                end                               

                                BfSF(bfCnt(4),2) = tmpLF;
                                bfCnt(4) = bfCnt(4) + 1;
                            end
                        end

                        TotGen(4) = TotGen(4) + sum(tmpBuffer);

                        [BfSF, energyCmp, timeOver, txPkts, cntRet, delays, tau, discards] = selectiveForward(BfSF, bfCnt(4), stM, floor(stP(4)), tau, RadioType,bfMax);
                        TotEnergyConsmp(4) = TotEnergyConsmp(4) + energyCmp;
                        TotLoss(4) = TotLoss(4) + timeOver;
                        TotTxed(4) = TotTxed(4) + txPkts;
                        TotDelay(4) = TotDelay(4) + delays;
                        TotDiscard = TotDiscard + discards;
                        bfCnt(4) = cntRet;
                    end


                    %% One time delay
                     if mod(stM, stP(5)) == 0                        
               %         Save generated packets
                        tmpBuffer = mainArrivPkts(stM-stP(5)+1:stM);

                        for i=1:length(tmpBuffer)
                            for j=1:tmpBuffer(i)
                                BfOne(bfCnt(5),1) = stM-stP(5) + i;         % Measurement time

%                                if scenario == 
%                                    tmpLF = lifetimeSet(tA);
%                                else
                                if scenario == 3
                                    tmpLF = randi(lifetimeSet(tA));
                                elseif scenario == 4
                                    tmpRnd = binornd(1,0.1);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 5
                                    tmpRnd = binornd(1,0.3);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 6
                                    tmpRnd = binornd(1,0.5);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 7
                                    tmpRnd = binornd(1,0.7);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                else
                                    tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                end

                                BfOne(bfCnt(5),2) = tmpLF;
                                bfCnt(5) = bfCnt(5) + 1;
                            end
                        end

                        TotGen(5) = TotGen(5) + sum(tmpBuffer);

    %                     tmp = BfOne(:, 2) - (stM - BfOne(:, 1));
    %                     tmp = tmp(tmp > 0);
    %                     avgLM =  mean(tmp);
    %                     stdLM =  std(tmp);
    %                     val = floor(avgLM + 2*stdLM);
    %                     if isnan(val) == 1
    %                         stP(5) = tdelta;
    %                     else
    %                         stP(5) = val;
    %                     end
             
                        [BfOne, energyCmp, timeOver, txPkts, cntRet,delays, delayedPkts, nxtTime] = onetimeDelay(BfOne, bfCnt(5), stM, floor(stP(5)), RadioType,bfMax);
                        TotEnergyConsmp(5) = TotEnergyConsmp(5) + energyCmp;
                        TotLoss(5) = TotLoss(5) + timeOver;
                        TotTxed(5) = TotTxed(5) + txPkts;
                        TotDelay(5) = TotDelay(5) + delays;
                        TotalDelayedPkts = TotalDelayedPkts + delayedPkts;
                        bfCnt(5) = cntRet;
                        stP(5) = nxtTime;
                    end
                
                    
                end
             
              %% Fixed
                if mod(stM, stP(1)) == 0                        
                    % Save generated packets
                    tmpBuffer = mainArrivPkts(stM-stP(1)+1:stM);

                    for i=1:length(tmpBuffer)
                        for j=1:tmpBuffer(i)
                            BfFix(bfCnt(1),1) = stM-stP(1) + i;         % Measurement time

%                                if scenario == 
%                                    tmpLF = lifetimeSet(tA);
%                                else
                                if scenario == 3
                                    tmpLF = randi(lifetimeSet(tA));
                                elseif scenario == 4
                                    tmpRnd = binornd(1,0.1);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 5
                                    tmpRnd = binornd(1,0.3);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 6
                                    tmpRnd = binornd(1,0.5);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 7
                                    tmpRnd = binornd(1,0.7);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                else
                                    tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                end

                            BfFix(bfCnt(1),2) = tmpLF; %tdelta + tmpLF;
                            bfCnt(1) = bfCnt(1) + 1;
                        end
                    end

                    TotGen(1) = TotGen(1) + sum(tmpBuffer);

                    [BfFix, energyCmp, timeOver, txPkts, cntRet,delays] = fixedTX(BfFix, bfCnt(1), stM, RadioType,bfMax);
                    TotEnergyConsmp(1) = TotEnergyConsmp(1) + energyCmp;
                    TotLoss(1) = TotLoss(1) + timeOver;
                    TotTxed(1) = TotTxed(1) + txPkts;
                    TotDelay(1) = TotDelay(1) + delays;
                    bfCnt(1) = cntRet;
                end

                
              %% Cascade
                if mod(stM, floor(stP(2))) == 0                       

                    % Save generated packets
                    tmpBuffer = mainArrivPkts(stM-floor(stP(2))+1:stM);

                    for i=1:length(tmpBuffer)
                        for j=1:tmpBuffer(i)
                            BfCas(bfCnt(2),1) = stM-floor(stP(2)) + i;

%                                if scenario == 
%                                    tmpLF = lifetimeSet(tA);
%                                else
                                if scenario == 3
                                    tmpLF = randi(lifetimeSet(tA));
                                elseif scenario == 4
                                    tmpRnd = binornd(1,0.1);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 5
                                    tmpRnd = binornd(1,0.3);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 6
                                    tmpRnd = binornd(1,0.5);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                elseif scenario == 7
                                    tmpRnd = binornd(1,0.7);
                                    if tmpRnd == 1
                                        tmpLF = randi([1,5]);
                                    else
                                        tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                    end
                                else
                                    tmpLF = randi([lifetimeSet(1),lifetimeSet(tA)]);
                                end

                            BfCas(bfCnt(2),2) = tmpLF;
                            bfCnt(2) = bfCnt(2) + 1;
                        end
                    end

                    TotGen(2) = TotGen(2) +  sum(tmpBuffer) ;

                    
                    [BfCas, energyCmp, timeOver, txPkts, cntRet, delays] = cascadeTimeOut(BfCas, bfCnt(2), stM, RadioType,bfMax);
                    
                    TotEnergyConsmp(2) = TotEnergyConsmp(2) + energyCmp;
                    TotLoss(2) = TotLoss(2) + timeOver;
                    TotTxed(2) = TotTxed(2) + txPkts;
                    TotDelay(2) = TotDelay(2) + delays;
                    bfCnt(2) = cntRet;

                end

                              
             

                
                stM = stM + 1;

            end

          %% Add results to data structure
            if rd==1
                ResultEnergyConsmpCtrl(ld,tA,:) = TotEnergyConsmp(3);
                ResultBufDelayCtrl(ld,tA,:) = TotDelay(3)/TotTxed(3);
                ResultBufTOCtrl(ld,tA,:) = TotLoss(3) / TotGen(3);
                ResultBufTxedCtrl(ld,tA,:) = TotTxed(3);
                ResultUnitEnergyCtrl(ld,tA,:) = TotEnergyConsmp(3)/(TotGen(3) - TotLoss(3));

                ResultEnergyConsmpSF(ld,tA,:) = TotEnergyConsmp(4);
                ResultBufDelaySF(ld,tA,:) = TotDelay(4)/TotTxed(4);
                ResultBufTOSF(ld,tA,:) = TotLoss(4) / TotGen(4);  
                ResultBufTxedSF(ld,tA,:) = TotTxed(4);
                ResultUnitEnergySF(ld,tA,:) = TotEnergyConsmp(4)/(TotGen(4) - TotLoss(4));
                

                ResultEnergyConsmpOne(ld,tA,:) = TotEnergyConsmp(5);
                ResultBufDelayOne(ld,tA,:) = TotDelay(5)/TotTxed(5);
                ResultBufTOOne(ld,tA,:) = TotLoss(5) / TotGen(5);  
                ResultBufTxedOne(ld,tA,:) = TotTxed(5);
                ResultBufPktsDelayedOne(ld,tA,:) = TotalDelayedPkts;
                ResultUnitEnergyOne(ld,tA,:) = TotEnergyConsmp(5)/(TotGen(5) - TotLoss(5));
                
            end

            ResultEnergyConsmpFixed(ld,tA,rd) = TotEnergyConsmp(1);
            ResultBufDelayFixed(ld,tA,rd) = TotDelay(1)/TotTxed(1);
            ResultBufTOFixed(ld,tA,rd) = TotLoss(1) / TotGen(1);          
            ResultBufTxedFixed(ld,tA,rd) = TotTxed(1);
            ResultUnitEnergyFixed(ld,tA,:) = TotEnergyConsmp(1)/(TotGen(1) - TotLoss(1));

            ResultEnergyConsmpCas(ld,tA,rd)  = TotEnergyConsmp(2);
            ResultBufDelayCas(ld,tA,rd) = TotDelay(2)/TotTxed(2); %stP(2);
            ResultBufTOCas(ld,tA,rd) = TotLoss(2) / TotGen(2);    
            ResultBufTxedCas(ld,tA,rd) = TotTxed(2);
            ResultUnitEnergyCas(ld,tA,:) = TotEnergyConsmp(2)/(TotGen(2) - TotLoss(2));

                        
        end

    end

end

     if sc==1
         save scenario1March6.mat;
     elseif sc == 2
         save scenario2March6.mat;
     elseif sc == 3
         save scenario3March6.mat;
     elseif sc == 4
         save scenario4March6.mat;
     elseif sc == 5
         save scenario5March6.mat;
     elseif sc == 6
         save scenario6March6.mat;
     elseif sc == 7
         save scenario7March6.mat;
     end

    figure(9 + 30*sc)
    % Normalized energy consumption with fixed transmission instances
    denom = ResultEnergyConsmpFixed(numRate,:,1);
    plot(lifetimeSet, (ResultEnergyConsmpFixed(numRate,:,2)./denom)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
    hold on;
    plot(lifetimeSet, (ResultEnergyConsmpFixed(numRate,:,1)./denom)*100,'k--o','LineWidth', 3,'MarkerSize', 8);    
    plot(lifetimeSet, (ResultEnergyConsmpFixed(numRate,:,3)./denom)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   
    plot(lifetimeSet, (ResultEnergyConsmpFixed(numRate,:,4)./denom)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);  
    plot(lifetimeSet, (ResultEnergyConsmpOne(numRate,:,1)./denom)*100,'r--^','LineWidth', 3,'MarkerSize', 8);  
    
    xlabel('Time constraints (sec)');
    ylabel('Normalized energy consumption (%)');
    legend('Fixed=5sec', 'Fixed=10sec', 'Fixed=15sec', 'Fixed=20sec', 'OT');
    %title('Fixed rate and lifetime');
    set(gcf,'color','w');


    figure(10 + 30*sc)
    % lifetime vs. numTO with max lambda

    plot(lifetimeSet, ResultBufTOFixed(numRate,:,2)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   
    hold on;
    plot(lifetimeSet, ResultBufTOFixed(numRate,:,1)*100,'k--o','LineWidth', 3,'MarkerSize', 8);      
    plot(lifetimeSet, ResultBufTOFixed(numRate,:,3)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   
    plot(lifetimeSet, ResultBufTOFixed(numRate,:,4)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);
    plot(lifetimeSet, ResultBufTOOne(numRate,:,1)*100,'r--^','LineWidth', 3,'MarkerSize', 8);

    xlabel('Time constraints (sec)');
    ylabel('% of measurements expire');
    legend('Fixed=5sec', 'Fixed=10sec', 'Fixed=15sec', 'Fixed=20sec', 'OT');
    %title('Fixed rate and lifetime');
    set(gcf,'color','w');


    

    
%     figure(3 + 30*sc)
%     % lifetime vs. energy with max lambda
% 
%     denom = ResultEnergyConsmpFixed(numRate,:,1);
%     plot(lifetimeSet, (ResultEnergyConsmpFixed(numRate,:,1)./denom)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
%     hold on;
%     plot(lifetimeSet, (ResultEnergyConsmpCas(numRate,:,1)./denom)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
%     plot(lifetimeSet, (ResultEnergyConsmpCtrl(numRate,:,1)./denom)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % 
%     plot(lifetimeSet, (ResultEnergyConsmpSF(numRate,:,1)./denom)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);  
%     plot(lifetimeSet, (ResultEnergyConsmpOne(numRate,:,1)./denom)*100,'r--^','LineWidth', 3,'MarkerSize', 8);  
% 
%     xlabel('Time constraints (sec)');
%     ylabel('Normalized energy consumption (%)');
%     legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
%     %title('Fixed rate and lifetime');
%     set(gcf,'color','w');


    
%     figure(4 + 30*sc)
%     % lifetime vs. energy with min arrival rate
% 
%     denom = ResultEnergyConsmpFixed(1,:,1);
%     plot(lifetimeSet, (ResultEnergyConsmpFixed(1,:,1)./denom)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
%     hold on;
%     plot(lifetimeSet, (ResultEnergyConsmpCas(1,:,1)./denom)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
%     plot(lifetimeSet, (ResultEnergyConsmpCtrl(1,:,1)./denom)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % 
%     plot(lifetimeSet, (ResultEnergyConsmpSF(1,:,1)./denom)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);  
%     plot(lifetimeSet, (ResultEnergyConsmpOne(1,:,1)./denom)*100,'r--^','LineWidth', 3,'MarkerSize', 8);  
% 
%     xlabel('Time constraints (sec)');
%     ylabel('Normalized energy consumption (%)');
%     legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
%     %title('Fixed rate and lifetime');
%     set(gcf,'color','w');


    
    figure(1 + 30*sc)
    % lifetime vs. energy with max lambda

    denom = ResultUnitEnergyFixed(numRate,:,1);
    plot(lifetimeSet, (ResultUnitEnergyFixed(numRate,:,1)./denom)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
    hold on;
    plot(lifetimeSet, (ResultUnitEnergyCas(numRate,:,1)./denom)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
    plot(lifetimeSet, (ResultUnitEnergyCtrl(numRate,:,1)./denom)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % 
    plot(lifetimeSet, (ResultUnitEnergySF(numRate,:,1)./denom)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);  
    plot(lifetimeSet, (ResultUnitEnergyOne(numRate,:,1)./denom)*100,'r--^','LineWidth', 3,'MarkerSize', 8);  

    xlabel('max time constraints (sec)');
    ylabel('Normalized energy per bit');
    legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
    %title('Fixed rate and lifetime');
    set(gcf,'color','w');
    
    figure(2 + 30*sc )
    % lifetime vs. numTO with max lambda

    plot(lifetimeSet, ResultBufTOFixed(numRate,:,1)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
    hold on;
    plot(lifetimeSet, ResultBufTOCas(numRate,:,1)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
    plot(lifetimeSet, ResultBufTOCtrl(numRate,:,1)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   %

    plot(lifetimeSet, ResultBufTOSF(numRate,:,1)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);
    plot(lifetimeSet, ResultBufTOOne(numRate,:,1)*100,'r--^','LineWidth', 3,'MarkerSize', 8);


    xlabel('max time constraints (sec)');
    ylabel('% of measurements expired');
    legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
    %title('Fixed rate and lifetime');
    set(gcf,'color','w');

    
    figure(3 + 30*sc)
    % lifetime vs. energy with min arrival rate

    denom = ResultUnitEnergyFixed(1,:,1);
    plot(lifetimeSet, (ResultUnitEnergyFixed(1,:,1)./denom)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
    hold on;
    plot(lifetimeSet, (ResultUnitEnergyCas(1,:,1)./denom)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
    plot(lifetimeSet, (ResultUnitEnergyCtrl(1,:,1)./denom)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % 
    plot(lifetimeSet, (ResultUnitEnergySF(1,:,1)./denom)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);  
    plot(lifetimeSet, (ResultUnitEnergyOne(1,:,1)./denom)*100,'r--^','LineWidth', 3,'MarkerSize', 8);  

    xlabel('max time constraints (sec)');
    ylabel('Normalized energy per bit');
    legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
    %title('Fixed rate and lifetime');
    set(gcf,'color','w');
    
    figure(4 + 30*sc )
    % lifetime vs. numTO with max lambda

    plot(lifetimeSet, ResultBufTOFixed(1,:,1)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
    hold on;
    plot(lifetimeSet, ResultBufTOCas(1,:,1)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
    plot(lifetimeSet, ResultBufTOCtrl(1,:,1)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   %
    plot(lifetimeSet, ResultBufTOSF(1,:,1)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);
    plot(lifetimeSet, ResultBufTOOne(1,:,1)*100,'r--^','LineWidth', 3,'MarkerSize', 8);

    xlabel('max time constraints (sec)');
    ylabel('% of measurements expired');
    legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
    %title('Fixed rate and lifetime');
    set(gcf,'color','w');

    
    figure(5 + 30*sc)
    % lifetime vs. energy with max lambda

    denom = ResultUnitEnergyFixed(:,1,1);
    plot(arrivRate, (ResultUnitEnergyFixed(:,1,1)./denom)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
    hold on;
    plot(arrivRate, (ResultUnitEnergyCas(:,1,1)./denom)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
    plot(arrivRate, (ResultUnitEnergyCtrl(:,1,1)./denom)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % 
    plot(arrivRate, (ResultUnitEnergySF(:,1,1)./denom)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);  
    plot(arrivRate, (ResultUnitEnergyOne(:,1,1)./denom)*100,'r--^','LineWidth', 3,'MarkerSize', 8);  

    xlabel('Arrival rate (measurements/sec)');
    ylabel('Normalized energy per bit');
    legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
    %title('Fixed rate and lifetime');
    set(gcf,'color','w');


    figure(6 + 30*sc )
    % lifetime vs. numTO with max lambda

    plot(arrivRate, ResultBufTOFixed(:,1,1)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
    hold on;
    plot(arrivRate, ResultBufTOCas(:,1,1)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
    plot(arrivRate, ResultBufTOCtrl(:,1,1)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   %

    plot(arrivRate, ResultBufTOSF(:,1,1)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);
    plot(arrivRate, ResultBufTOOne(:,1,1)*100,'r--^','LineWidth', 3,'MarkerSize', 8);


    xlabel('Arrival rate (measurements/sec)');
    ylabel('% of measurements expired');
    legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
    %title('Fixed rate and lifetime');
    set(gcf,'color','w');
    
    
    figure(7 + 30*sc)
    % lifetime vs. energy with min arrival rate

    denom = ResultUnitEnergyFixed(:,length(lifetimeSet),1);
    plot(arrivRate, (ResultUnitEnergyFixed(:,length(lifetimeSet),1)./denom)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
    hold on;
    plot(arrivRate, (ResultUnitEnergyCas(:,length(lifetimeSet),1)./denom)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
    plot(arrivRate, (ResultUnitEnergyCtrl(:,length(lifetimeSet),1)./denom)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % 
    plot(arrivRate, (ResultUnitEnergySF(:,length(lifetimeSet),1)./denom)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);  
    plot(arrivRate, (ResultUnitEnergyOne(:,length(lifetimeSet),1)./denom)*100,'r--^','LineWidth', 3,'MarkerSize', 8);  

    xlabel('Arrival rate (measurements/sec)');
    ylabel('Normalized energy per bit');
    legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
    %title('Fixed rate and lifetime');
    set(gcf,'color','w');
 
    figure(8 + 30*sc )
    % lifetime vs. numTO with max lambda

    plot(arrivRate, ResultBufTOFixed(:,length(lifetimeSet),1)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
    hold on;
    plot(arrivRate, ResultBufTOCas(:,length(lifetimeSet),1)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
    plot(arrivRate, ResultBufTOCtrl(:,length(lifetimeSet),1)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   %

    plot(arrivRate, ResultBufTOSF(:,length(lifetimeSet),1)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);
    plot(arrivRate, ResultBufTOOne(:,length(lifetimeSet),1)*100,'r--^','LineWidth', 3,'MarkerSize', 8);


    xlabel('Arrival rate (measurements/sec)');
    ylabel('% of measurements expired');
    legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
    %title('Fixed rate and lifetime');
    set(gcf,'color','w');
    
    
    
    
%     figure(7 + 30*sc)
%     % lifetime vs. delay with max lambda
% 
%     plot(lifetimeSet, ResultBufDelayFixed(numRate,:,1), 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
%     hold on;
%     plot(lifetimeSet, ResultBufDelayCas(numRate,:,1),'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
%     plot(lifetimeSet, ResultBufDelayCtrl(numRate,:,1), 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % 
%     plot(lifetimeSet, ResultBufDelaySF(numRate,:,1),'k-.d','LineWidth', 3,'MarkerSize', 8);       % 
%     plot(lifetimeSet, ResultBufDelayOne(numRate,:,1),'r--^','LineWidth', 3,'MarkerSize', 8);       % 
% 
%     xlabel('Time constraints (sec)');
%     ylabel('Buffering delay (sec)');
%     legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
%     %title('Fixed rate and lifetime');
%     set(gcf,'color','w');
%     % 
%     
%     figure(8 + 30*sc)
%     % lifetime vs. delay with max lambda
% 
%     plot(lifetimeSet, ResultBufDelayFixed(1,:,1), 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
%     hold on;
%     plot(lifetimeSet, ResultBufDelayCas(1,:,1),'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
%     plot(lifetimeSet, ResultBufDelayCtrl(1,:,1), 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % 
%     plot(lifetimeSet, ResultBufDelaySF(1,:,1),'k-.d','LineWidth', 3,'MarkerSize', 8);       % 
%     plot(lifetimeSet, ResultBufDelayOne(1,:,1),'r--^','LineWidth', 3,'MarkerSize', 8);       % 
% 
%     xlabel('Time constraints (sec)');
%     ylabel('Buffering delay (sec)');
%     legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
%     %title('Fixed rate and lifetime');
%     set(gcf,'color','w');
%     % 
% 
% 
%     figure(9 + 30*sc)
%     % arrival rate vs. delay with min lifetime
% 
%     plot(arrivRate, ResultBufDelayFixed(:,1,1), 'b-+', 'LineWidth', 3,'MarkerSize', 8);   % Fix
%     hold on;
%     plot(arrivRate, ResultBufDelayCas(:,1,1),'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
%     plot(arrivRate, ResultBufDelayCtrl(:,1,1), 'c--', 'LineWidth', 3,'MarkerSize', 8);   % 
%     plot(arrivRate, ResultBufDelaySF(:,1,1),'k-.d','LineWidth', 3,'MarkerSize', 8);       % 
%     plot(arrivRate, ResultBufDelayOne(:,1,1),'r--^','LineWidth', 3,'MarkerSize', 8);    
% 
%     xlabel('Arrival rate (packets/sec)');
%     ylabel('Buffering delay (sec)');
%     legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
%     %title('Fixed rate and lifetime');
%     set(gcf,'color','w');
%     
%     
%     figure(10 + 30*sc)
%     % arrival rate vs. delay with min lifetime
% 
%     plot(arrivRate, ResultBufDelayFixed(:,length(lifetimeSet),1), 'b-+', 'LineWidth', 3,'MarkerSize', 8);   % Fix
%     hold on;
%     plot(arrivRate, ResultBufDelayCas(:,length(lifetimeSet),1),'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
%     plot(arrivRate, ResultBufDelayCtrl(:,length(lifetimeSet),1), 'c--', 'LineWidth', 3,'MarkerSize', 8);   % 
%     plot(arrivRate, ResultBufDelaySF(:,length(lifetimeSet),1),'k-.d','LineWidth', 3,'MarkerSize', 8);       % 
%     plot(arrivRate, ResultBufDelayOne(:,length(lifetimeSet),1),'r--^','LineWidth', 3,'MarkerSize', 8);    
% 
%     xlabel('Arrival rate (packets/sec)');
%     ylabel('Buffering delay (sec)');
%     legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
%     %title('Fixed rate and lifetime');
%     set(gcf,'color','w');
    
    
%     figure(11 + 30*sc)
%     % arrival rate vs. energy with min lifetime
%     denom = ResultEnergyConsmpFixed(:,1,1);
%     plot(arrivRate, ResultEnergyConsmpFixed(:,1,1), 'b-+', 'LineWidth', 3,'MarkerSize', 8);   % Fix
%     hold on;
%     plot(arrivRate, ResultEnergyConsmpCas(:,1,1),'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
%     plot(arrivRate, ResultEnergyConsmpCtrl(:,1,1), 'c--', 'LineWidth', 3,'MarkerSize', 8);   % Ctrl
%     plot(arrivRate, ResultEnergyConsmpSF(:,1,1),'k-.d','LineWidth', 3,'MarkerSize', 8);       % SF
%     plot(arrivRate, ResultEnergyConsmpOne(:,1,1),'r--^','LineWidth', 3,'MarkerSize', 8);      
% 
%     xlabel('Arrival rate (packets/sec)');
%     ylabel('Normalized energy consumption (%)');
%     legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
%     %title('Fixed rate and lifetime');
%     set(gcf,'color','w');
    
%     figure(12 + 30*sc)
%     % arrival rate vs. energy with min lifetime
%     denom = ResultEnergyConsmpFixed(:,length(lifetimeSet),1);
%     plot(arrivRate, (ResultEnergyConsmpFixed(:,length(lifetimeSet),1)./denom)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8);   % Fix
%     hold on;
%     plot(arrivRate, (ResultEnergyConsmpCas(:,length(lifetimeSet),1)./denom)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
%     plot(arrivRate, (ResultEnergyConsmpCtrl(:,length(lifetimeSet),1)./denom)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8);   % Ctrl
%     plot(arrivRate, (ResultEnergyConsmpSF(:,length(lifetimeSet),1)./denom)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);       % SF
%     plot(arrivRate, (ResultEnergyConsmpOne(:,length(lifetimeSet),1)./denom)*100,'r--^','LineWidth', 3,'MarkerSize', 8);      
% 
%     xlabel('Arrival rate (packets/sec)');
%     ylabel('Normalized energy consumption (%)');
%     legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
%     %title('Fixed rate and lifetime');
%     set(gcf,'color','w');
    
    
    
    
       

    
    
end  % End scenario





% %%

% 
% 
% 
% figure(10 )
% % lifetime vs. numTO with max lambda
% 
% denom = ResultBufTxedFixed(numRate,:,1);
% plot(lifetimeSet, (ResultBufTxedFixed(numRate,:,1)./denom)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
% hold on;
% plot(lifetimeSet, (ResultBufTxedCas(numRate,:,1)./denom)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
% plot(lifetimeSet, (ResultBufTxedCtrl(numRate,:,1)./denom)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % 
% plot(lifetimeSet, (ResultBufTxedSF(numRate,:,1)./denom)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);  
% plot(lifetimeSet, (ResultBufTxedOne(numRate,:,1)./denom)*100,'r--^','LineWidth', 3,'MarkerSize', 8);  
% 
% 
% xlabel('Time constraints (sec)');
% ylabel('# of measurements txed');
% legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
% %title('Fixed rate and lifetime');
% set(gcf,'color','w');
% 
% 
% 
% 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 
% 
% figure(3 )
% % arrival rate vs. energy with min lifetime
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultEnergyConsmpFixed(1,1,i);
% end
% plot(distToSink, tmp, 'b-+', 'LineWidth', 3,'MarkerSize', 8);   % Fix
% hold on;
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultEnergyConsmpCas(1,1,i);
% end
% plot(distToSink, tmp,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultEnergyConsmpCtrl(1,1,i);
% end
% plot(distToSink, tmp, 'c--', 'LineWidth', 3,'MarkerSize', 8);   % ctrl
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultEnergyConsmpSF(1,1,i);
% end
% plot(distToSink, tmp,'k-.d','LineWidth', 3,'MarkerSize', 8);       % sf
% 
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultEnergyConsmpOne(1,1,i);
% end
% plot(distToSink, tmp,'r--^','LineWidth', 3,'MarkerSize', 8);       % sf
% 
% xlabel('Distance to sink (Hops)');
% ylabel('Energy consumption (mW)');
% legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
% %title('Fixed rate and lifetime');
% set(gcf,'color','w');
% 
% 
% 

% 
% 
% figure(6 )
% % distance vs. delay with min lifetime and max arrival rate
% 
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufDelayFixed(1,2,i);
% end
% plot(distToSink, tmp, 'b-+', 'LineWidth', 3,'MarkerSize', 8);   % Fix
% 
% 
% hold on;
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufDelayCas(1,2,i);
% end
% plot(distToSink, tmp,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
% 
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufDelayCtrl(1,1,i);
% end
% plot(distToSink, tmp, 'c--', 'LineWidth', 3,'MarkerSize', 8);   % 
% 
% 
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufDelaySF(1,1,i);
% end
% plot(distToSink, tmp,'k-.d','LineWidth', 3,'MarkerSize', 8);       % 
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufDelayOne(1,1,i);
% end
% plot(distToSink, tmp,'r--^','LineWidth', 3,'MarkerSize', 8);   
% 
% 
% 
% xlabel('Distance to sink (Hops)');
% ylabel('Buffering delay (sec)');
% legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
% %title('Fixed rate and lifetime');
% set(gcf,'color','w');
% 
% 
% 
% 
% %         ResultEnergyConsmpCtrl = zeros(numRate,typesOfApp,length(distToSink));
% 
% figure(8 )
% % distance to sink vs. numTO with min lifetime and max arrival rate
% 
% plot(arrivRate, ResultBufTOFixed(:,1,1)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8);   % Fix
% hold on;
% plot(arrivRate, ResultBufTOCas(:,1,1)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
% plot(arrivRate, ResultBufTOCtrl(:,1,1)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8);   % 
% plot(arrivRate, ResultBufTOSF(:,1,1)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);    
% plot(arrivRate, ResultBufTOOne(:,1,1)*100,'r--^','LineWidth', 3,'MarkerSize', 8);    
% 
% 
% xlabel('Arrival rate (packets/sec)');
% ylabel('% of packets expire');
% legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
% %title('Fixed rate and lifetime');
% set(gcf,'color','w');
% 
% 
% 
% 
% figure(9 )
% % arrival rate vs. numTO with min lifetime
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufTOFixed(1,1,i);
% end    
% 
% plot(distToSink, tmp*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8);   % Fix
% hold on;
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufTOCas(1,1,i);
% end
% plot(distToSink, tmp*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufTOCtrl(1,1,i);
% end    
% 
% plot(distToSink, tmp*100, 'c--', 'LineWidth', 3,'MarkerSize', 8);   % 
% hold on;
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufTOSF(1,1,i);
% end
% plot(distToSink, tmp*100,'k-.d','LineWidth', 3,'MarkerSize', 8);       % 
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufTOOne(1,1,i);
% end
% plot(distToSink, tmp*100,'r--^','LineWidth', 3,'MarkerSize', 8);    
% 
% xlabel('Distance to sink (Hops)');
% ylabel('% of packets expire');
% legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
% %title('Fixed rate and lifetime');
% set(gcf,'color','w');



%%
% 
% %         ResultEnergyConsmpCtrl = zeros(numRate,typesOfApp,length(distToSink));
% 
% figure(11 )
% % distance to sink vs. numTO with min lifetime and max arrival rate
% 
% plot(arrivRate, ResultBufTxedFixed(:,1,1)*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8);   % Fix
% hold on;
% plot(arrivRate, ResultBufTxedCas(:,1,1)*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
% plot(arrivRate, ResultBufTxedCtrl(:,1,1)*100, 'c--', 'LineWidth', 3,'MarkerSize', 8);   %
% plot(arrivRate, ResultBufTxedSF(:,1,1)*100,'k-.d','LineWidth', 3,'MarkerSize', 8);   
% plot(arrivRate, ResultBufTxedOne(:,1,1)*100,'r--^','LineWidth', 3,'MarkerSize', 8);   
% 
% xlabel('Arrival rate (packets/sec)');
% ylabel('# of measurements txed');
% legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
% %title('Fixed rate and lifetime');
% set(gcf,'color','w');
% 
% 
% 
% 
% figure(12 )
% % arrival rate vs. numTO with min lifetime
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufTxedFixed(1,1,i);
% end    
% 
% plot(distToSink, tmp*100, 'b-+', 'LineWidth', 3,'MarkerSize', 8);   % Fix
% hold on;
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufTxedCas(1,1,i);
% end
% plot(distToSink, tmp*100,'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufTxedCtrl(1,1,i);
% end    
% 
% plot(distToSink, tmp*100, 'c--', 'LineWidth', 3,'MarkerSize', 8);   % 
% hold on;
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufTxedSF(1,1,i);
% end
% plot(distToSink, tmp*100,'k-.d','LineWidth', 3,'MarkerSize', 8);       % 
% 
% tmp = zeros(length(distToSink),1);
% for i=1:length(distToSink)
%     tmp(i) = ResultBufTxedOne(1,1,i);
% end
% plot(distToSink, tmp*100,'r--^','LineWidth', 3,'MarkerSize', 8);    
% 
% xlabel('Distance to sink (Hops)');
% ylabel('# of measurements txed');
% legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
% %title('Fixed rate and lifetime');
% set(gcf,'color','w');
% 
%  
 
 
 %
%  
% figure(30)
% tmp = ResultBufTOFixed(numRate,:,2);
% plot(lifetimeSet, (ResultEnergyConsmpOne(numRate,:,3))*100, 'r--', 'LineWidth', 2,'MarkerSize', 8);   % Fixed lifetime
% hold on;
% plot(lifetimeSet, (ResultEnergyConsmpOne(numRate,:,4))*100, 'k-.d', 'LineWidth', 2,'MarkerSize', 8);   % Random lifetime
% plot(lifetimeSet, (ResultEnergyConsmpFixed(numRate,:,3))*100, 'k-+', 'LineWidth', 2,'MarkerSize', 8);   % Random lifetime
% plot(lifetimeSet, (ResultEnergyConsmpFixed(numRate,:,4))*100, 'b-o', 'LineWidth', 2,'MarkerSize', 8);   % Random lifetime
% 
% xlabel('Time constraint (sec)');
% ylabel('Energy consumption (mW)');
% %title('Fixed rate and lifetime');
% legend('OT-Fix', 'OT-Random','Fix-15sec', 'Random-15sec')
% %legend('Fixed:25sec', 'Random:25sec','Fixed:15sec', 'Random:15sec')
% set(gcf,'color','w');
%  
% figure(31)
% plot(lifetimeSet, (ResultBufTOOne(numRate,:,3))*100, 'r--', 'LineWidth', 2,'MarkerSize', 8);   % Fixed lifetime
% hold on;
% plot(lifetimeSet, (ResultBufTOOne(numRate,:,4))*100, 'k-.d', 'LineWidth', 2,'MarkerSize', 8);   % Random lifetime
% plot(lifetimeSet, (ResultBufTOFixed(numRate,:,3))*100, 'k-+', 'LineWidth', 2,'MarkerSize', 8);   % Random lifetime
% plot(lifetimeSet, (ResultBufTOFixed(numRate,:,4))*100, 'b-o', 'LineWidth', 2,'MarkerSize', 8);   % Random lifetime
% 
% xlabel('Time constraint (sec)');
% ylabel('% of packets expire');
% %title('Fixed rate and lifetime');
% legend('OT-Fix', 'OT-Random','Fix-15sec', 'Random-15sec')
% set(gcf,'color','w'); 


% % %% Fix one parameter and varying remaining paramters
% % %ResultEnergyConsmpFixed = zeros(numRate,typesOfApp,length(distToSink));  
% % figure(20)
% %     plot(lifetimeSet, ResultEnergyConsmpCtrl(1,:,1), 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
% %     hold on;
% %     plot(lifetimeSet, ResultEnergyConsmpCtrl(5,:,1),'k-.d','LineWidth', 3,'MarkerSize', 8);       % cascade
% %     plot(lifetimeSet, ResultEnergyConsmpCtrl(10,:,1),'k-+','LineWidth', 3,'MarkerSize', 8);       % C-F
% %     plot(lifetimeSet, ResultEnergyConsmpCtrl(15,:,1),'k-^', 'LineWidth', 3,'MarkerSize', 8);        % S-F
% %     plot(lifetimeSet, ResultEnergyConsmpCtrl(20,:,1),'k-o', 'LineWidth', 3,'MarkerSize', 8);        % Mono
% % 
% %   
% %     xlabel('Time constraints (sec)');
% %     ylabel('Energy consumption (mW)');
% %     legend('Rate=0.1', 'Rate=0.5','Rate=1','Rate=1.5', 'Rate=2');
% %     title('CL');
% %     set(gcf,'color','w');
% %     
% % 
% %     figure(21)
% %     % arrival rate vs. energy with min lifetime
% % 
% %     plot(arrivRate, ResultEnergyConsmpCtrl(:,1,1), 'c--', 'LineWidth', 3,'MarkerSize', 8);   
% %     hold on;
% %     plot(arrivRate, ResultEnergyConsmpCtrl(:,2,1),'k-.d','LineWidth', 3,'MarkerSize', 8);    
% %     plot(arrivRate, ResultEnergyConsmpCtrl(:,3,1),'k-+','LineWidth', 3,'MarkerSize', 8);     
% %     plot(arrivRate, ResultEnergyConsmpCtrl(:,4,1),'k-^', 'LineWidth', 3,'MarkerSize', 8);    
% %     plot(arrivRate, ResultEnergyConsmpCtrl(:,5,1),'k-o', 'LineWidth', 3,'MarkerSize', 8);    
% %     plot(arrivRate, ResultEnergyConsmpCtrl(:,6,1),'k-', 'LineWidth', 3);
% % 
% % 
% %     xlabel('Arrival rate (packets/sec)');
% %     ylabel('Energy consumption (mW)');
% %     legend('LF=5', 'LF=10','LF=15', 'LF=20','LF=25','LF=30');
% %     title('CL');
% %     set(gcf,'color','w');
% %     
% % 
% % 
% %     figure(22)
% %     plot(lifetimeSet, ResultEnergyConsmpSF(1,:,1), 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
% %     hold on;
% %     plot(lifetimeSet, ResultEnergyConsmpSF(5,:,1),'k-.d','LineWidth', 3,'MarkerSize', 8);       % cascade
% %     plot(lifetimeSet, ResultEnergyConsmpSF(10,:,1),'k-+','LineWidth', 3,'MarkerSize', 8);       % C-F
% %     plot(lifetimeSet, ResultEnergyConsmpSF(15,:,1),'k-^', 'LineWidth', 3,'MarkerSize', 8);        % S-F
% %     plot(lifetimeSet, ResultEnergyConsmpSF(20,:,1),'k-o', 'LineWidth', 3,'MarkerSize', 8);        % Mono
% % 
% %   
% %     xlabel('Time constraints (sec)');
% %     ylabel('Energy consumption (mW)');
% %     legend('Rate=0.1', 'Rate=0.5','Rate=1','Rate=1.5', 'Rate=2');
% %     title('SF');
% %     set(gcf,'color','w');
% %     
% % 
% %     figure(23)
% %     % arrival rate vs. energy with min lifetime
% % 
% %     plot(arrivRate, ResultEnergyConsmpSF(:,1,1), 'c--', 'LineWidth', 3,'MarkerSize', 8);   
% %     hold on;
% %     plot(arrivRate, ResultEnergyConsmpSF(:,2,1),'k-.d','LineWidth', 3,'MarkerSize', 8);    
% %     plot(arrivRate, ResultEnergyConsmpSF(:,3,1),'k-+','LineWidth', 3,'MarkerSize', 8);     
% %     plot(arrivRate, ResultEnergyConsmpSF(:,4,1),'k-^', 'LineWidth', 3,'MarkerSize', 8);    
% %     plot(arrivRate, ResultEnergyConsmpSF(:,5,1),'k-o', 'LineWidth', 3,'MarkerSize', 8);    
% %     plot(arrivRate, ResultEnergyConsmpSF(:,6,1),'k-', 'LineWidth', 3);
% % 
% % 
% %     xlabel('Arrival rate (packets/sec)');
% %     ylabel('Energy consumption (mW)');
% %     legend('LF=5', 'LF=10','LF=15', 'LF=20','LF=25','LF=30');
% %     title('SF');
% %     set(gcf,'color','w');
% % 
% % 
% %     figure(20+5)
% %     plot(lifetimeSet, ResultEnergyConsmpFixed(1,:,1), 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
% %     hold on;
% %     plot(lifetimeSet, ResultEnergyConsmpFixed(5,:,1),'k-.d','LineWidth', 3,'MarkerSize', 8);       % cascade
% %     plot(lifetimeSet, ResultEnergyConsmpFixed(10,:,1),'k-+','LineWidth', 3,'MarkerSize', 8);       % C-F
% %     plot(lifetimeSet, ResultEnergyConsmpFixed(15,:,1),'k-^', 'LineWidth', 3,'MarkerSize', 8);        % S-F
% %     plot(lifetimeSet, ResultEnergyConsmpFixed(20,:,1),'k-o', 'LineWidth', 3,'MarkerSize', 8);        % Mono
% % 
% %   
% %     xlabel('Time constraints (sec)');
% %     ylabel('Energy consumption (mW)');
% %     legend('Rate=0.1', 'Rate=0.5','Rate=1','Rate=1.5', 'Rate=2');
% %     title('Fixed');
% %     set(gcf,'color','w');
% %     
% % 
% %     figure(21+5)
% %     % arrival rate vs. energy with min lifetime
% % 
% %     plot(arrivRate, ResultEnergyConsmpFixed(:,1,1), 'c--', 'LineWidth', 3,'MarkerSize', 8);   
% %     hold on;
% %     plot(arrivRate, ResultEnergyConsmpFixed(:,2,1),'k-.d','LineWidth', 3,'MarkerSize', 8);    
% %     plot(arrivRate, ResultEnergyConsmpFixed(:,3,1),'k-+','LineWidth', 3,'MarkerSize', 8);     
% %     plot(arrivRate, ResultEnergyConsmpFixed(:,4,1),'k-^', 'LineWidth', 3,'MarkerSize', 8);    
% %     plot(arrivRate, ResultEnergyConsmpFixed(:,5,1),'k-o', 'LineWidth', 3,'MarkerSize', 8);    
% %     plot(arrivRate, ResultEnergyConsmpFixed(:,6,1),'k-', 'LineWidth', 3);
% % 
% % 
% %     xlabel('Arrival rate (packets/sec)');
% %     ylabel('Energy consumption (mW)');
% %     legend('LF=5', 'LF=10','LF=15', 'LF=20','LF=25','LF=30');
% %     title('Fixed');
% %     set(gcf,'color','w');
% %     
% % 
% % 
% %     figure(22+5)
% %     plot(lifetimeSet, ResultEnergyConsmpCas(1,:,1), 'c--', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
% %     hold on;
% %     plot(lifetimeSet, ResultEnergyConsmpCas(5,:,1),'k-.d','LineWidth', 3,'MarkerSize', 8);       % cascade
% %     plot(lifetimeSet, ResultEnergyConsmpCas(10,:,1),'k-+','LineWidth', 3,'MarkerSize', 8);       % C-F
% %     plot(lifetimeSet, ResultEnergyConsmpCas(15,:,1),'k-^', 'LineWidth', 3,'MarkerSize', 8);        % S-F
% %     plot(lifetimeSet, ResultEnergyConsmpCas(20,:,1),'k-o', 'LineWidth', 3,'MarkerSize', 8);        % Mono
% % 
% %   
% %     xlabel('Time constraints (sec)');
% %     ylabel('Energy consumption (mW)');
% %     legend('Rate=0.1', 'Rate=0.5','Rate=1','Rate=1.5', 'Rate=2');
% %     title('Cascading');
% %     set(gcf,'color','w');
% %     
% % 
% %     figure(23+5)
% %     % arrival rate vs. energy with min lifetime
% % 
% %     plot(arrivRate, ResultEnergyConsmpCas(:,1,1), 'c--', 'LineWidth', 3,'MarkerSize', 8);   
% %     hold on;
% %     plot(arrivRate, ResultEnergyConsmpCas(:,2,1),'k-.d','LineWidth', 3,'MarkerSize', 8);    
% %     plot(arrivRate, ResultEnergyConsmpCas(:,3,1),'k-+','LineWidth', 3,'MarkerSize', 8);     
% %     plot(arrivRate, ResultEnergyConsmpCas(:,4,1),'k-^', 'LineWidth', 3,'MarkerSize', 8);    
% %     plot(arrivRate, ResultEnergyConsmpCas(:,5,1),'k-o', 'LineWidth', 3,'MarkerSize', 8);    
% %     plot(arrivRate, ResultEnergyConsmpCas(:,6,1),'k-', 'LineWidth', 3);
% % 
% % 
% %     xlabel('Arrival rate (packets/sec)');
% %     ylabel('Energy consumption (mW)');
% %     legend('LF=5', 'LF=10','LF=15', 'LF=20','LF=25','LF=30');
% %     title('Cascading');
% %     set(gcf,'color','w');
% % 
% 
% %%
%      
% %     figure(1 )
% %     % lifetime vs. energy with max lambda
% % 
% %     plot(lifetimeSet, ResultEnergyConsmpFixed(numRate,:,1), 'b-+', 'LineWidth', 3,'MarkerSize', 8 );   % Fix
% %     hold on;
% %     plot(lifetimeSet, ResultEnergyConsmpCas(numRate,:,1),'k--o','LineWidth', 3,'MarkerSize', 8);       % cascade
% %   
% %     xlabel('Time constraints (sec)');
% %     ylabel('Energy consumption (mW)');
% %     legend('Fixed', 'Cas', 'CL', 'SF', 'OT');
% %     %title('Fixed rate and lifetime');
% %     set(gcf,'color','w');
% 
 

%%


