function out1 = reward(delay,DST)

% Shannon channel capacity

N = 1;
alpha = 0.5;

%dbm = [0.11:0.01:20];       % 10 * log10 (SNR)

%snr = 10.^(dbm./10);

%C = 0.5 .* log2(1 + snr);   % Capacity, bits per second
%R = alpha .* C;             % reliably transmitted bps

%s = 1./R;                   % transmission time
s = delay;

w = s .* N .* (2.^(2 ./ (alpha.*s)) -1)

% The energy consumption decrease with delay
% This is energy overhead: e(delay) - e(max delay) \approx e(delay) - 0
% Increase delay decrease the probability to packet drop
% Increase delay decrease the probablity of buffer stable
% Fixed bufMax, increase bufCnt

P_arr = 1/(DST+delay);
%P_arr = 1/delay;


%out1 = exp(-w * DST)* P_arr;
out1 = exp(w);
