
function [out1 out2 out3] = energyModel(radioType, numPkt)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Energy consumption model
% Realistic power consumption model
% CC1000 @443MHz, CC1000 868MHz, CC2420 @2.4GHz
% Watt = Jourle / sec
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The power unit is mW
if numPkt ~= 0
    
    PowerModel = [22.2 28.8 59.1;
                15.9 25.8 26.5;
                10 5 1];

    P_R0 = PowerModel(1,radioType);
    P_T0 = PowerModel(2,radioType);
    P_max = PowerModel(3,radioType);

    Efficiency = [15.7 6.4 3.7]./100;

    eta = Efficiency(radioType);
    alpha = 2;                          % Fading constant
    epsilon = 0.0005;                   % P_{rx_min} * A
    dist=100;                           % 100m

    minTxRate = 610;                   % bits per second



    % Power
    P_tx = P_T0 + (epsilon* dist^alpha)/eta;
    P_rx = P_R0;

    % Time
    packetSize = numPkt * 4;                            % each measuremen has 4 byte 
    packetSize = packetSize + 4 + 1 + 1 + 2 + 8 + 2;    % header + etc..
    packetSize = packetSize * 8;                        % Bits
    t_tx = packetSize / minTxRate; 
    t_rx = t_tx * 0.5;
    % Energy consumption for transmission
    calResult = P_rx*t_rx + P_tx*t_tx;
else
    calResult = 0;
    P_rx = 0;
    P_tx = 0;
    t_rx = 0;
    t_tx = 0;
end



out1 = calResult;
out2 = P_rx*t_rx;
out3 = P_tx*t_tx;

% 
% P_tx = 
% Hop = [ 1 2 3 4 5 6 7 8];
% 
% R = [1:400];
% P_T_dist = zeros(length(R), length(Hop));
% 
% for j=1:length(Hop)
%     for i=1:length(R)
%         P_T_dist(i,j) = (Hop(j)-1)*P_R0(1) + Hop(j)*P_T0(1) + (Hop(j) * epsilon * (R(i)/Hop(j))^alpha) ./ Efficiency(1);
%     end
% end
% 
% P_R = P_R0;
% 
% figure(13)
% for j=1:length(Hop)
%     plot(R, P_T_dist(:,j))
%     hold on;
% end
