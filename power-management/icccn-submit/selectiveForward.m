function [out1 out2 out3 out4 out5 out6 out7 out8] = selectiveForward(buf, bufCnt, currTime, decisionInterval, tau, RadioType, bfMax)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Selective forwarding: transmit a message > threshold, discard <= threshold
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

maxHop = 9;

numTO = 0;                                  % number of time out pkts
numTX = 0;                                  % number of pkts transmit
TotDelay = 0;
numDiscard = 0;

K = currTime / decisionInterval;

[xx e_rx e_tx] = energyModel(RadioType, 1);

tau_new = 0;

for i=1:bufCnt-1
    
    importance = buf(i,2);
    tau_new = (1- 1/K) * tau + max( importance - e_tx*tau, 0)/(K*e_rx);
    thrld = e_tx * tau_new;
    
    if (currTime - buf(i,1)) > buf(i,2)            % expired packet
        numTO = numTO + 1;
        buf(i,:) = -1;                              % mark it expired
    elseif importance > thrld    
            TotDelay = TotDelay + (currTime-buf(i,1));
            numTX = numTX + 1;                          % packet will transmit
            buf(i,:) = -1;
    else
            % discard packet
            buf(i,:) = -1;
            numDiscard = numDiscard  + 1;
            numTO = numTO + 1;
    end
    
    
end

% Energy consumption
[conSump xx yy] = energyModel(RadioType, numTX);

% Buffer update: delete expired packets
retBuf = buf(buf(1:bufCnt-1,1) ~= -1, :);

buf2 = zeros(bfMax,2);
ll = size(retBuf);
for i=1:ll(1)
    buf2(i,:) = retBuf(i,:);
end


out1 = buf2;
out2 = conSump;
out3 = numTO;
out4 = numTX;
out5 = ll(1)+1;                       % point out next empty buffer
out6 = TotDelay;
out7 = tau_new;
out8 = numDiscard;