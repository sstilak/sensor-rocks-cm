function [out1 out2 out3 out4 out5 out6] = cascadeTimeOut(buf,bufCnt,currTime,RadioType,bfMax)

%[BfCas, energyCmp, timeOver, cntRet] = cascadeTimeOut(BfCas, bfCnt(2), stM, RadioType);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% cascadeTimeOut: each node waits 2e, e = sampling interval - (shd*hop)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Check the timeout of each packet

numTO = 0;                                  % number of time out pkts
numTX = 0;                                  % number of pkts transmit

TotDelay = 0;

for i=1:bufCnt-1
    
    if (currTime - buf(i,1)) > buf(i,2)            % expired packet
        numTO = numTO + 1;
        buf(i,:) = -1;                              % mark it expired
    else
        TotDelay = TotDelay + (currTime-buf(i,1));
        numTX = numTX + 1;                          % packet will transmit
        buf(i,:) = -1;   
    end
    
    
end

% Energy consumption
[conSump xx yy] = energyModel(RadioType, numTX);

% Buffer update: delete expired packets

retBuf = buf(buf(1:bufCnt-1,1) ~= -1, :);

buf2 = zeros(bfMax,2);
ll = size(retBuf);
for i=1:ll(1)
    buf2(i,:) = retBuf(i,:);
end


%BfCas, energyCmp, timeOver, txOver, cntRet, delay

out1 = buf2;
out2 = conSump;
out3 = numTO;
out4 = numTX;
out5 = ll(1)+1;                       % point out next empty buffer
out6 = TotDelay;
