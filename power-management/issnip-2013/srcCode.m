clear all;
close all;

set(0,'DefaultAxesFontSize', 14)
set(0,'DefaultFigureWindowStyle','docked');
set(0,'DefaultAxesFontName', 'Arial')

linestyles = cellstr(char('-k',':k','-.k','--k','-k',':k','-.k','--k','-k',':k','-k',':k',...
'-.k','--k','-k',':k','-.k','--k','-k',':k','-.k'));

w = [3 3 2 2 ];
% Performance evaluation of previous works

% Harvesting energy
WI_WINTER= csvread('USCRN_WISCONSIN_WINTER.csv');
WI_SUMMER = csvread('USCRN_WISCONSIN_SUMMER.csv');
CA_WINTER= csvread('USCRN_SANTA_WINTER.csv');
CA_SUMMER = csvread('USCRN_SANTA_SUMMER.csv');
TX_WINTER= csvread('USCRN_AUSTIN_WINTER.csv');
TX_SUMMER = csvread('USCRN_AUSTIN_SUMMER.csv');


% Discrete time models
D = 3;				
hoursOfDay = 24;								% referrable past days		
dayOfYear = D+7;

unitPwr = 8.96;							% Consumed Watt

% BoostingLevel = 10; 						% sampling every 10 second
% BoostingInterval = 60; 						 % Level of boosting interval


% WorkLoads = [0.1:0.1:1];       							% workload
% varSampling = 0.15;
				

numTimeSlot = hoursOfDay*floor((60/10));

UserReqInterval = [1 2 3 4 6 12 24]*6;							% sec, Frequency of user request messabe
endPointLevel = [0.1:0.1:1];										% �̸��� endPointlevel ����� high reuquest �� frequency
Iteration = 25;




% We assume user request follows normal distribution, we only use absolute values
freqHigh = 0.3;


% Get real data

sonode = csvread('sonode.CSV');
snode = sonode(:,2)';

sensingData = zeros(1, length(snode)*60);

count = 1;
for j=1:length(snode)
	for i=1:60
		
		sensingData(1,count) = snode(1,j);
		count = count + 1;
		
	end
end


tK = 12;													% Max. number of today's referrable time slots
alpha = 0.95;

pastDays = zeros(D, numTimeSlot);

RpredE = zeros(dayOfYear-D , numTimeSlot , 6);					% predicted energy
RrealE = zeros(dayOfYear-D , numTimeSlot , 6);					% real energy
userRequst = zeros(dayOfYear-D, numTimeSlot);


%%% WCMA  : this operate every epoch. The epoch can be a day or a shorter time slot
dataHarvest = zeros(dayOfYear, numTimeSlot, 6);		% 6 is the number of set

for i = 1:dayOfYear
	count=1;

	for j=hoursOfDay*(i-1)+1:hoursOfDay*(i-1)+hoursOfDay
		
		for tmp=1:6
			dataHarvest( i , count ,1) = WI_WINTER( j , 3 );
			% dataHarvest( i , count ,2) = WI_WINTER( j , 3 );
			% dataHarvest( i , count ,3) = CA_WINTER( j , 3 );
			% dataHarvest( i , count ,4) = CA_SUMMER( j , 3 );
			% dataHarvest( i , count ,5) = TX_WINTER( j , 3 );
			% dataHarvest( i , count ,6) = TX_SUMMER( j , 3 );
			count = count + 1;
		end
		
	end
	
end


for i=D+1:dayOfYear
	
	% Estimate harvested level
	for numHS=1:1
	
		% Create past records
		count = 1;
		
		for j=i-D:i-1
		
			pastDays(count,:) = dataHarvest(j,:,numHS);
			count = count + 1;
			
		end
		
		for j=2:numTimeSlot
						
			Eprev = dataHarvest(i,j-1,numHS);						% Previous slot
				
			Md = sum(pastDays(1:D,j))/D;					% Current slot's past days average
			
			if j-tK < 1
				startJ = 1;
			else
				startJ = j-tK;
			end
			
			
			V = zeros(1,j-startJ);
			
			count=0;
			for iK=startJ:j-1

				count = count +1;
				
				if sum(dataHarvest(i-D:i-1,iK,numHS)) == 0
					V(count) = 0;
				else
					V(count) = dataHarvest(i,iK,numHS)/sum(dataHarvest(i-D:i-1,iK,numHS));
				end
				
			end
			
			P = [1:length(V)]./length(V);						% weighting vector	

			GAP = (V*P')/sum(P);
			
			RpredE(i-D,j,numHS) = alpha*Eprev + (1-alpha)*Md*GAP;
			RrealE(i-D,j,numHS) = dataHarvest(i,j,numHS);
			
		end
	
	end
		
	
	
end

% plot(RrealE(15,:), '-k');
% hold on;
% plot(RpredE(15,:));

%%%%%%%% End of WCMA



%%%%%%%%%% End get real data

tableDeathTimeslot       = zeros(length(endPointLevel), length(UserReqInterval));
tableDeathTimeslotProp   = zeros(length(endPointLevel),  length(UserReqInterval));

tableRMSE = zeros(length(endPointLevel), length(UserReqInterval));
tableRMSEProp = zeros(length(endPointLevel),  length(UserReqInterval));

tableAvgPwr = zeros( length(endPointLevel),  length(UserReqInterval) );
tableAvgPwrProp = zeros(length(endPointLevel),  length(UserReqInterval));


% Results are stored in 

SumtableDeathTimeslot       = zeros(length(endPointLevel), length(UserReqInterval));
SumtableDeathTimeslotProp   = zeros(length(endPointLevel),  length(UserReqInterval));

SumtableRMSE = zeros(length(endPointLevel), length(UserReqInterval));
SumtableRMSEProp = zeros(length(endPointLevel),  length(UserReqInterval));

SumtableAvgPwr = zeros( length(endPointLevel),  length(UserReqInterval) );
SumtableAvgPwrProp = zeros(length(endPointLevel),  length(UserReqInterval));


for itr=1:Iteration

	for k=1:length(UserReqInterval)  

		for btc=1:length(endPointLevel)
			
			%% Scenario generation
			
			% rndFreq = rand;
			
			userRequst = zeros(dayOfYear-D, numTimeSlot );
			
			lastPtr = 1;
			
			for i=1+D:dayOfYear
				
				% overDay = randi(numTimeSlot, [1,floor(endPointLevel(btc)*numTimeSlot)]);
				
				% overDay = sort(overDay, 'ascend');
				
				
				for j=1:numTimeSlot
				
					userRequst(i-D,j) = 10/10;       % Sensing every 10 min
		
				end
				
				
				overSlot = [];
				for tmp=1:numTimeSlot
					
					if mod(tmp, UserReqInterval(k)) == 1
						overSlot = [overSlot tmp];
					end
					
				end
				
				% Now we have the each time slot's start point
				% Determine whether high request will be occur or no
					
				overPtr = randi(length(overSlot), [1,floor(endPointLevel(btc)*length(overSlot))]);
				
				
				for tmp=1:length(overPtr)
					
					
					
					if overSlot(overPtr(tmp))+3 <= numTimeSlot
						userRequst(i-D,overSlot(overPtr(tmp)):overSlot(overPtr(tmp))+3) = 10/1;
					else
						userRequst(i-D,overSlot(overPtr(tmp)):numTimeSlot) = 10/1;
					end

					
					
				end
				
				% over = 0;
					
					% if mod(j, UserReqInterval(k)) == 1		% Generate high fruqeuncy
						
						% if j+3 <= numTimeSlot
							% userRequst(i-D,j:j+3) = 10/1;
						% else
							% userRequst(i-D,j:numTimeSlot) = 10/1;
						% end
					% end
					
					% % for ltmp=1:length(overDay)
					
						% % if j == overDay(ltmp)
							% % over = 1;
						% % end
						
					% % end
					
					
					% % if over == 1
						
						% % userRequst(i-D,j) = 10/1;
						
					% % end
							
				
				
			end

			userRequestPwr = userRequst*unitPwr;
			
			
			%% Calculate targetData
			
			%%%%%%%%%%%%% STANDARD DATA %%%%%%%%%%%%%%%
			userRequestInterval = userRequst * 60;			% convert to second

			[row col] = size(userRequestInterval);
			
			targetPoint = [];

			for i=1:row

				targetPoint = [ targetPoint userRequestInterval(i,:)];
				
			end

			for j=2:row*col
				targetPoint(1,j) = targetPoint(1,j-1) + targetPoint(1,j) ;
			end

			targetData = [];


			lastPtr=1;
			for i=1:length(targetPoint)
				
				if targetPoint(i) <= length(sensingData)
				
					for j=lastPtr:targetPoint(i)
						targetData = [targetData sensingData(1, targetPoint(i) )];
					end
					
					lastPtr = targetPoint(i) + 1;
				else
					for j=lastPtr:length(sensingData)
						targetData = [targetData sensingData(1, length(sensingData) )];
					end
					break;
				end
				
			end

			%%%% Now I have target data

			% Battery level record
			Blevel = zeros(dayOfYear-D, numTimeSlot);	
			BlevelProp = zeros(dayOfYear-D,numTimeSlot);

			
			% Progressive Filling Algorithm

			sizeB = (12-11.1)*55*3600;											
			delta = 5; 												% the amount of increment
			maxB = 1*sizeB;												% Maximum battery size
			initB = sizeB;
			targetB = 0;											% Target battery size

			
			s = zeros(dayOfYear-D,numTimeSlot);								% numTimeSlotAllocated energy spending

			for i=D+1:dayOfYear
						
				% for j=1:interval:numTimeSlot
			
				aAfix = ones(1, numTimeSlot);			% The number of actual time slot 
				
				% if i ~= D+1
					% initB = initB + sum(RrealE(i-D-1,:,1)) - sum(s(i-D-1, :));
				% end

				while( sum(aAfix) > 0 )
					
					for j=1:numTimeSlot
						
						if aAfix(j) == 1		% Not yet determined
							
							tmpS = s(i-D , :);
							tmpS(j) = tmpS(j) + delta;
							
							B = initB;
							valid = 1;												% 0 = FALSE / 1 = TRUE
							
							
							for tmp=2:numTimeSlot+1
								B = min(maxB, B + RpredE(i-D,tmp-1,1) - tmpS(tmp-1));
							end
							
							if B < targetB
								valid = 0;				% FALSE
							end
							
							%% End check_validity
							
							if valid == 1
								s(i-D,j) = tmpS(j);
							else
								aAfix(j) = 0;
							end
							
						end
					end
					
				end

							
				if i ~= D+1  
					Blevel(i-D,1) = Blevel(i-D-1,numTimeSlot);
				else
					Blevel(i-D,1) = initB;
				end
				
				for j=2:numTimeSlot+1
				
					Blevel(i-D,j) = min(maxB, Blevel(i-D,j-1) + RrealE(i-D,tmp-1,1) - s(i-D, j-1));
					
					if j~=numTimeSlot+1
						
						if Blevel(i-D,j) < targetB
							s(i-D, j) = 0;
							Blevel(i-D,j) = 0;
						end

					end
									
				end
				
				initB = Blevel(i-D,numTimeSlot);
				
				
							
				
				%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				
				% tableDeathTimeslot      = zeros(length(endPointLevel), length(UserReqInterval), Iteration);
				% tableRMSE 				= zeros(length(endPointLevel), length(UserReqInterval), Iteration);
				% tableAvgPwr 			= zeros( length(endPointLevel),  length(UserReqInterval) , Iteration);
				
				tableDeathTimeslot(btc,k) =  tableDeathTimeslot(btc,k) + sum(s(i-D,:)<=0);
				tableAvgPwr(btc,k) = tableAvgPwr(btc,k) + mean(s(i-D,:));
					
			end
			
			tableDeathTimeslot(btc,k) = 100*(tableDeathTimeslot(btc,k) / (dayOfYear*numTimeSlot));	% Store percentage
			tableAvgPwr(btc,k) = tableAvgPwr(btc,k) / dayOfYear;
			
			
			% Calculate RMSE
			
			
			%%%%%%%%%  S1  
			[row col] = size(s);
			
			sPoint = [];

			for i=1:row

				sPoint = [ sPoint ceil(s(i,:))];
				
			end
			
			for j=2:row*col
				sPoint(1,j) = sPoint(1,j-1) + sPoint(1,j) ;
			end
			
			sData = zeros(1,length(targetData));
			
			lastPtr=1;
			
			count = 1;
			for i=1:length(sPoint)
				
				if sPoint(i) <= length(targetData)
				
					for j=lastPtr:sPoint(i)
						sData(1,count) = sensingData(1, sPoint(i) );
						count = count + 1;
					end
					
					lastPtr = sPoint(i) + 1;
				else
					for j=lastPtr:length(targetData)
						sData(1,count) = sensingData(1, length(targetData) );
						count = count + 1;
					end
					
					break;
				end
				
			end
			
			tableRMSE(btc,k) = sqrt(sum(abs(targetData - sData).* abs(targetData - sData) ) / length(targetData));
			
			
			%%%%% End Calculation
					
				
		
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			%%%%%%%                           Proposed algorithm
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			
			sizeB = (12-11.1)*55*3600;											
			delta = 5; 												% the amount of increment
			maxB = 1*sizeB;												% Maximum battery size
			initB = sizeB;
			targetB = 0;											% Target battery size

			
			Bsaved = 0;
			maxBsaved = 0.000001 * sizeB;		% 1% of the batery

			sProp = zeros(dayOfYear-D,numTimeSlot);								% numTimeSlotAllocated energy spending
			sProp2 = zeros(dayOfYear-D,numTimeSlot);							% New allocation 

			for i=D+1:dayOfYear
				
				over = 0;												% 0 = FALSE / 1 = TRUE
				overPtr = [];

				while( over == 0 )
					
						sProp(i-D,:) = sProp(i-D,:) + delta;
						
						%valid = check_validity(sProp);
						
						B = initB;
						
						interval = UserReqInterval(k)/(5*60); 
						
						for tmp=2:(numTimeSlot)+1
							
							B = min(maxB, B + RpredE(i-D,tmp-1,1) - sProp(i-D, tmp-1));
							
							if B < targetB
								overPtr = [overPtr tmp];
							end
							
						end
						
						if B < targetB
							over = 1;				% TRUE
						end
						
						%% End check_validity
						
						if over == 1
							% decrease sProp
							sProp(i-D, overPtr(length(overPtr)): numTimeSlot) = sProp(i-D, overPtr(length(overPtr)): numTimeSlot) - delta;
						end
						
				end
				
				
				% userRequst = zeros(dayOfYear-D, numTimeSlot);
				%% Treat user request
				sProp2(i-D , : ) = sProp(i-D , : );
				
				for j=1:numTimeSlot
					
					if userRequestPwr(i-D,j) < sProp2(i-D,j)

						% Save battery and store the remaining to Bsaving
						Bsaved = Bsaved + (sProp2(i-D,j) - userRequestPwr(i-D,j));
						
						if Bsaved > maxBsaved
							Bsaved = maxBsaved;
						end
						
						sProp2(i-D,j) = userRequestPwr(i-D,j);
						
					else	% userRequestPwr(i-D,j) > sProp2(i-D,j)
						
						if userRequestPwr(i-D,j) > sProp2(i-D,j) + Bsaved  
							sProp2(i-D,j) =sProp2(i-D,j)+ Bsaved; % + Bsaved 
							Bsaved = Bsaved - Bsaved;
							
							if Bsaved <= 0
								Bsaved = 0;
							end
							
						else
							
							Bsaved = Bsaved - ( userRequestPwr(i-D,j) - sProp2(i-D,j) );
							sProp2(i-D,j) = userRequestPwr(i-D,j);
							
							if Bsaved <= 0
								Bsaved = 0;
							end
							
						end
						
						
						
					end
					
				end
				
				
				if i ~= D+1  
					BlevelProp(i-D,1) = BlevelProp(i-D-1,numTimeSlot);
				else
					BlevelProp(i-D,1) = initB;
				end
				
				for j=2:numTimeSlot+1
				
					BlevelProp(i-D,j) = min(maxB, BlevelProp(i-D,j-1) + RrealE(i-D,j-1,1) - sProp2(i-D, j-1));
					
					if j~=numTimeSlot+1
						
						if BlevelProp(i-D,j) < targetB
							sProp2(i-D, j) = 0;
							BlevelProp(i-D,j) = 0;
						end

					end
									
				end
				initB = BlevelProp(i-D,numTimeSlot);
				
			%%%%%%%%%%%%%%%%%%%%%%%%%%
			
			
				tableDeathTimeslotProp(btc,k) =  tableDeathTimeslotProp(btc,k) + sum(sProp2(i-D,:)<=0);
				tableAvgPwrProp(btc,k) = tableAvgPwrProp(btc,k) + mean(sProp2(i-D,:));
					
			end
			
			tableDeathTimeslotProp(btc,k) = 100*(tableDeathTimeslotProp(btc,k) / (dayOfYear*numTimeSlot));	% Store percentage
			tableAvgPwrProp(btc,k) = tableAvgPwrProp(btc,k) / dayOfYear;
			
			
			% Calculate RMSE
			
			
			%%%%%%%%%  Props 
			[row col] = size(sProp2);
			
			sProp2Point = [];

			for i=1:row

				sProp2Point = [ sProp2Point ceil(sProp2(i,:))];
				
			end
			
			for j=2:row*col
				sProp2Point(1,j) = sProp2Point(1,j-1) + sProp2Point(1,j) ;
			end
			
			sProp2Data = zeros(1,length(targetData));
			
			lastPtr=1;
			
			count = 1;
			for i=1:length(sProp2Point)
				
				if sProp2Point(i) <= length(targetData)
				
					for j=lastPtr:sProp2Point(i)
						sProp2Data(1,count) = sensingData(1, sProp2Point(i) );
						count = count + 1;
					end
					
					lastPtr = sProp2Point(i) + 1;
				else
					for j=lastPtr:length(targetData)
						sProp2Data(1,count) = sensingData(1, length(targetData) );
						count = count + 1;
					end
					
					break;
				end
				
			end
			
			tableRMSEProp(btc,k) = sqrt(sum(abs(targetData - sProp2Data).* abs(targetData - sProp2Data) ) / length(targetData));
			
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5555
			
		end % for btc=1:length(endPointLevel)
		
		SumtableDeathTimeslot(:,k)  		 = 	SumtableDeathTimeslot(:,k) + tableDeathTimeslot(:,k);   
		SumtableDeathTimeslotProp(:,k)		= SumtableDeathTimeslotProp(:,k) + tableDeathTimeslotProp(:,k);

		SumtableRMSE(:,k)				 = SumtableRMSE(:,k) + tableRMSE(:,k);
		SumtableRMSEProp(:,k)			 = SumtableRMSEProp(:,k) + tableRMSEProp(:,k);

		SumtableAvgPwr(:,k) 			= SumtableAvgPwr(:,k) + tableAvgPwr(:,k);
		SumtableAvgPwrProp(:,k)			 = SumtableAvgPwrProp(:,k) + tableAvgPwrProp(:,k);
	
		%%%%%%%%%%%%%%%%%%%%%%%
		
	end	% for k=1:length(UserReqInterval) 
	
	
end		% for itr=1:Iteration 

SumtableDeathTimeslot  		 = 	SumtableDeathTimeslot ./ Iteration;
SumtableDeathTimeslotProp	= SumtableDeathTimeslotProp./ Iteration;

SumtableRMSE		 = SumtableRMSE./ Iteration;
SumtableRMSEProp	 = SumtableRMSEProp./ Iteration;

SumtableAvgPwr 		= SumtableAvgPwr./ Iteration;
SumtableAvgPwrProp	 = SumtableAvgPwrProp./ Iteration;


save('GoodMorning.mat');
	