clear;
fprintf('\nStart Time [%s]\n',datestr(now,'HH:MM:SS'));

% chdir 'D:\UCSD\MATLAB\RESULT_sensor\';
path(path,'/home/jiy011/Dropbox/Participated Project/Bojan Papers/Work_Bojan/Work_Bojan/code/resources/nway_toolbox')
load 'CIMIS_dataPar_3D.mat';
% load 'CIMIS_corr3.mat'

% t_week = 744;
% % k = [2 4 6 8 10 20 50 100];
k = 50;

n_fold = 1;
sparsity = [ 0.1 0.2 0.333 0.4]; % 0.5 0.667 0.75 0.8 0.9];
% sparsity = [ 0.5 ];
mean_sp = 0.5;

init = 0;
initFac = 0;
% DATA_3DN(:,1,:) = DATA_3DN(:,7,:);
% DATA_3D(:,1,:) = DATA_3D(:,7,:);
% DATA_3DN(:,7,:) = [];
% DATA_3D(:,7,:) = [];
% 
% data_mean(:,1) = data_mean(:,7);
% data_mean(:,7) = [];
% data_std(:,1) = data_std(:,7);
% data_std(:,7) = [];

data_n = DATA_3DN(:,1:6,1:2);   % Only 6 sensors
data = DATA_3D(:,1:6,1:2);        % only 6 sensors
% mean = data_mean;
% std = data_std;
clear DATA_3DN DATA_3D;



[nt, ns, nx] = size(data_n);
tot = nt*ns*nx;


for ik = 1:length(k)        % length(k) = 1
    
    for isens = 1:1
        
%         if isens == 5
%             break;
%         end
        
%         if isens == 2
%             start = 3;
%         else
%             start = 1;
%         end
        
        fprintf('\n $$$  Sensor %d\n', isens);

        for isp = 4:length(sparsity)        
    
            for irnd = 1:n_fold         % n_fold=1

                if init == 1
                    load Tmp_fold_save.mat;
                    irnd = size(NRMSE_mean_test_rnd{isp,ik}, 2)+1;
                    init = 0;
                end
                
                %%%% sparsity ENTRIS MISSING AT RANDOM
                prm = randperm(tot);        % tot = nt*ns*nx;
                index = round(tot*mean_sp); % mean_sp = 0.5

                idx_t = zeros(tot,1);
                idx_t(prm(1:index)) = 1;

                nn = 0;

                for j=1:ns
                    for x=1:nx

                        idx_t_3d(:,j,x) = idx_t(nn*nt+1:(nn+1)*nt); % get 0 or 1 value
                        nn = nn+1;
                    end
                end
                
                % 
                %%%% sparsity ENTRIS MISSING AT RANDOM
                prm_s = randperm(nt*nx);
                index = round(nt*nx*sparsity(isp));

                idx_ts = zeros(nt*nx,1);
                idx_ts(prm_s(1:index)) = 1;

                nn = 0;
                idx_3d = idx_t_3d;
                idx_tmp = reshape(idx_ts, nt,1,nx);
                idx_3d(:,isens,:) = idx_tmp;
                if isens == 4                           % Only for Sensor 4
                    idx_3d(:,isens+1,:) = idx_tmp;
                end
                
%                 if isens == 1
%                     idx_3d(:,isens,:) = idx_tmp;
% %                     idx_3d(:,2,:) = idx_tmp;
% %                     idx_3d(:,3,:) = idx_tmp;
%                 elseif isens ==2
%                     idx_3d(:,2,:) = idx_tmp;                
%                 else
%                     idx_3d(:,3,:) = idx_tmp;
%                 end
                
                % Test data and train data are orthogonal
                TEST_DATA = data_n(idx_3d==1);
                TRAIN_DATA = data_n;
                TRAIN_DATA(idx_3d==1) = NaN;
            
%                 [A, B, C] = myParFac_cv(TRAIN_DATA, TEST_DATA, idx_3d, k(ik), init);
                % Train and Test are orthogonal
                % idx_3d indicate existing element of test data
                % k = 50 and ik=1, k(ik) = 50
                % init = 0;
                % C_tmean, C_smean, C_xmean: correlation matrix of data
                % there exits global correlation map for given data set
                [A, B, C] = myParFac_pri_tmp_sens_loc(TRAIN_DATA, TEST_DATA, idx_3d, k(ik), init, C_tmean, C_smean, C_xmean);
                
                % What is size of A,B, and C?
                data_pn = zeros(nt,ns,nx);

                for t=1:nt
                    for j=1:ns
                        for x=1:nx

                            data_pn(t,j,x) = A(t,:).*B(j,:)*C(x,:)';

                        end
                    end
                end

            
                for is = 1:ns       % o
                                        
                    sensor_data_pn(:,:) = data_pn(:,is,:);
                    sensor_data(:,:) = data(:,is,:);
                    sensor_data_p = (sensor_data_pn .* repmat(data_std(is),nt,nx)) + repmat(data_mean(is),nt,nx); 
                    
                    mx = max(sensor_data(:));
                    mn = min(sensor_data(:));
                    
                    sensor_err = (sensor_data - sensor_data_p).^2;
                    all_err = sensor_err(:);
                    NRMSE_sensor_r{ik,isens}{isp}(irnd,is) = sqrt(nanmean(all_err)) / (mx - mn);
                    
%                     if is == isens
                   
                    sensor_idx(:,:) = idx_3d(:,is,:);
                    sensor_data_test = sensor_data(sensor_idx==1);
                    sensor_data_p_test = sensor_data_p(sensor_idx==1);

                    sensor_err_test = (sensor_data_test - sensor_data_p_test).^2;
                    all_err_test = sensor_err_test(:);
                    NRMSE_sensor_test_r{ik,isens}{isp}(irnd,is) = sqrt(nanmean(all_err_test)) / (mx - mn);
                        
%                     end

                end
            
                NRMSE_mean_r{ik,isens}{isp}(irnd) = mean(NRMSE_sensor_r{ik,isens}{isp}(irnd,:));
                NRMSE_mean_test_r{ik,isens}{isp}(irnd) = mean(NRMSE_sensor_test_r{ik,isens}{isp}(irnd,:));

                result_r.NRMSE_sensor_r = NRMSE_sensor_r;
                result_r.NRMSE_sensor_test_r = NRMSE_sensor_test_r;
                result_r.NRMSE_mean_r = NRMSE_mean_r;
                result_r.NRMSE_mean_test_r = NRMSE_mean_test_r;

                save 'result_r_(6)4-9.mat' result_r;
                
    %             save 'Tmp_fold_save.mat' NRMSE_rnd NRMSE_rnd NRMSE_mean_rnd NRMSE_mean_test_rnd irnd ik isp

                fprintf('\n     ## Sparsity = %.2f, k = %d, ROUND = %d | NRMSE; %.3f \n', sparsity(isp), k(ik), irnd, NRMSE_mean_test_r{ik,isens}{isp}(irnd));
            
            end


            NRMSE_sensor{ik,isens}(isp) =  mean(NRMSE_sensor_r{ik,isens}{isp}(:,:));
            NRMSE_sensor_test{ik,isens}(isp) =  mean(NRMSE_sensor_test_r{ik,isens}{isp}(:,:));
            
            NRMSE_mean{ik,isens}(isp) =  mean(NRMSE_mean_r{ik,isens}{isp}(:));
            NRMSE_mean_test{ik,isens}(isp) =  mean(NRMSE_mean_test_r{ik,isens}{isp}(:,:));

            
%             NRMSE{ik,isens}(isp) = mean(NRMSE_rnd{ik,isens}{isp});
%             NRMSE_mean{ik,isens}{isp} = mean(NRMSE{ik,isens}{isp});
%             NRMSE_std{ik,isens}{isp} = std(NRMSE{ik,isens}{isp});
% 
%             NRMSE_test{ik,isens}{isp} = (NRMSE_test_rnd{ik,isens}{isp});
%             NRMSE_mean_test{ik,isens}{isp} = mean(NRMSE_test{ik,isens}{isp});
%             NRMSE_std_test{ik,isens}{isp} = std(NRMSE_test{ik,isens}{isp});


            fprintf('\n     ### Sparsity = %.2f, k = %d | MEAN NRMSE TEST: %.3f \n', sparsity(isp), k(ik), NRMSE_mean_test{ik,isens}(isp));

            result.NRMSE_sensor = NRMSE_sensor;
            result.NRMSE_sensor_test = NRMSE_sensor_test;
            result.NRMSE_mean = NRMSE_mean;
            result.NRMSE_mean_test = NRMSE_mean_test;

            save 'result_sensors_(6)4-9.mat' result;

        
        end
    
    end

%     NRMSE_sens(:,1) = cell2mat(result.NRMSE_mean_test{1,1}');
%     NRMSE_sens(:,2) = cell2mat(result.NRMSE_mean_test{1,2}');
%     NRMSE_sens(:,3) = cell2mat(result.NRMSE_mean_test{1,3}');
%     NRMSE_sens(:,4) = cell2mat(result.NRMSE_mean_test{1,4}');
%     NRMSE_sens(:,5) = cell2mat(result.NRMSE_mean_test{1,5}');
%     NRMSE_sens(:,6) = cell2mat(result.NRMSE_mean_test{1,6}');
% %     NRMSE_sens(:,7) = cell2mat(result.NRMSE_mean_test{1,7}');
% 
% %     NRMSE_s3(:,1) = mean(NRMSE_sens(:,1:3)');
% %     NRMSE_s3(:,2) = mean(NRMSE_sens(:,4:5)');
% %     NRMSE_s3(:,3) = mean(NRMSE_sens(:,6)');
%     
%     save 'CIMIS_sp_3D_k_rnd_sens3.mat' result k NRMSE_sens;
%     save 'CIMIS_sparsity_3D.mat' A B C err_p_sens err_pn_test;
    %err_p_mean(ik) = sqrt(nanmean(err_p{ik}(:)));
    %err_p_mean{ik} = sqrt(nanmean(err_p{ik}));

%     DATA_PN{ik} = data_pn;
%     DATA_P{ik} = data_p;
end

