clear;
fprintf('\nStart Time [%s]\n',datestr(now,'HH:MM:SS'));

load 'CIMIS_dataPar_3D.mat';
% load 'CIMIS_corr3.mat'
% load 'ANR_data_3D_week_10.mat';

% t_week = 744;
% k = [2 4 6 8 10 20 50 100];
k = [50];

n_fold = 1;
% sparsity = [ 0.1 0.2 0.333 0.4 0.5 0.667 0.75 0.8 0.9];
% sparsity = [ 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9];
sparsity = 0.8;
block = [118, 140, 164, 188, 212];
block_start = 60;

init = 0;
initFac = 0;

data_n = DATA_3DN;
data = DATA_3D;
% mean = data_mean;
% std = data_std;

clear DATA_3DN DATA_3D;

[nt, ns, nx] = size(data_n);
tot = nt*ns*nx;

%%%% check percentile!!!
% temp1 = data(:,2,:);
% tt = temp1(:);
% prc1 = prctile(tt, [1, 99]);
% 
% mx = max(tt);
% mn = min(tt);
% n1 = mx - mn;
% n2 = prc1(2) - prc1(1);


for isp = 1:length(sparsity)
    
    for ik = 1:length(block)
    
        for irnd = 1:n_fold

            if init == 1
                load Tmp_fold_save.mat;
                irnd = size(NRMSE_mean_test_rnd{isp,ik}, 2)+1;
                init = 0;
            end
            
            prm = randperm(nt*nx);
            index = round(nt*nx*sparsity(isp));

            idx_t = zeros(nt*nx,1);
            idx_t(prm(1:index)) = 1;
            idx_2 = reshape(idx_t,nt,1,nx);
            
            for jj = 1:ns
                idx_3d(:,jj,:) = idx_2;
            end

            prm2 = randperm(nx);
            
%             idx_3 = zeros(nt,1);
%             idx_3(block_start:block_start+block,1) = 1;
%             idx_3 = zeros(nt,1,nx);
%             idx_3(:,1,prm(1)) = idx_t;
            
%             index = round(nt*nx*sparsity(isp));
% 
%             idx_t = zeros(nt*nx,1);
%             idx_t(prm(1:index)) = 1;
%             idx_2 = reshape(idx_t,nt,1,nx);
            
            for jj = 1:ns
                idx_3d(block_start:block_start+block(ik),jj,prm2(1)) = 1;
            end
            
            TEST_DATA = data_n(idx_3d==1);
            TRAIN_DATA = data_n;
            TRAIN_DATA(idx_3d==1) = NaN;
            
%             [A, B, C] = myParFac_cv(TRAIN_DATA, TEST_DATA, idx_3d, k(ik), init);
            [A, B, C] = myParFac_pri_tmp_sens_loc(TRAIN_DATA, TEST_DATA, idx_3d, k, init, C_tmean, C_smean, C_xmean);
        
            data_pn = zeros(nt,ns,nx);

            for t=1:nt
                for j=1:ns
                    for x=1:nx

                        data_pn(t,j,x) = A(t,:).*B(j,:)*C(x,:)';

                    end
                end
            end

            
            for is = 1:ns
                sensor_data_n(:,:) = data_pn(:,is,:);
                sensor_idx(:,:) = idx_3d(:,is,:);

                sensor_data_p = (sensor_data_n .* repmat(data_std(is),nt,nx)) + repmat(data_mean(is),nt,nx); 
                sensor_data_p_test = sensor_data_p(sensor_idx==1);

                sensor_data(:,:) = data(:,is,:);
                sensor_data_test = sensor_data(sensor_idx==1);
                
%                 prc = prctile(sensor_data(:), [1,99]);
%                 mx = prc(2);
%                 mn = prc(1);
                mx = max(sensor_data(:));
                mn = min(sensor_data(:));

                
                sensor_err = (sensor_data - sensor_data_p).^2;
                all_err = sensor_err(:);
                NRMSE_rnd{isp,ik}(irnd,is) = sqrt(nanmean(all_err)) / (mx - mn);

                sensor_err_test = (sensor_data_test - sensor_data_p_test).^2;
                all_err_test = sensor_err_test(:);
                NRMSE_test_rnd{isp,ik}(irnd,is) = sqrt(nanmean(all_err_test)) / (mx - mn);

            end
            
            NRMSE_mean_rnd{isp,ik}(irnd) = mean(NRMSE_rnd{isp,ik}(irnd,:));
            NRMSE_mean_test_rnd{isp,ik}(irnd) = mean(NRMSE_test_rnd{isp,ik}(irnd,:));
            
            AA{isp,ik,irnd} = A;
			BB{isp,ik,irnd} = B;
			CC{isp,ik,irnd} = C;
            IDX{isp,ik,irnd} = idx_3d;
            
            result_r.NRMSE_sensor_r = NRMSE_rnd;
            result_r.NRMSE_sensor_test_r = NRMSE_test_rnd;
            result_r.NRMSE_mean_r = NRMSE_mean_rnd;
            result_r.NRMSE_mean_test_r = NRMSE_mean_test_rnd;

            save 'result_block_r-118-212.mat' result_r AA BB CC IDX;
            
            save 'Tmp_fold_save.mat' NRMSE_rnd NRMSE_rnd NRMSE_mean_rnd NRMSE_mean_test_rnd irnd ik isp
            
            fprintf('\n     ## Sparsity = %.2f, k = %d, ROUND = %d | NRMSE TEST: %.3f \n', sparsity(isp), k, irnd, NRMSE_mean_test_rnd{isp,ik}(irnd));
            
        end
        
        
        NRMSE{isp,ik} = mean(NRMSE_rnd{isp,ik});
        NRMSE_mean(isp,ik) = mean(NRMSE{isp,ik});
        NRMSE_std(isp,ik) = std(NRMSE{isp,ik});
        
        NRMSE_test{isp,ik} = mean(NRMSE_test_rnd{isp,ik});
        NRMSE_mean_test(isp,ik) = mean(NRMSE_test{isp,ik});
        NRMSE_std_test(isp,ik) = std(NRMSE_test{isp,ik});
        

        fprintf('\n     ### Sparsity = %.2f, k = %d | MEAN NRMSE TEST: %.3f \n', sparsity(isp), k, NRMSE_mean_test(isp,ik));

        
        result.NRMSE_sensor = NRMSE;
        result.NRMSE_sensor_test = NRMSE_test;
        result.NRMSE_mean = NRMSE_mean;
        result.NRMSE_mean_test = NRMSE_mean_test;

        save 'result_block-118-212.mat' result  AA BB CC IDX;

%         result.NRMSE{isp,ik} = NRMSE{isp,ik};
%         result.NRMSE_mean(isp,ik) = NRMSE_mean(isp,ik);
%         result.NRMSE_std(isp,ik) = NRMSE_std(isp,ik);
%         result.NRMSE_test{isp,ik} = NRMSE_test{isp,ik};
%         result.NRMSE_mean_test(isp,ik) = NRMSE_mean_test(isp,ik);
%         result.NRMSE_std_test(isp,ik) = NRMSE_std_test(isp,ik);
%         
%         save 'CIMIS_sp_3D_k_row_pri.mat' result AA BB CC IDX k;
        
%         load CIMIS_sp_3D_k_rnd_pri_ALL.mat
   
%         result_ALL.NRMSE{isp,ik} = NRMSE{isp,ik};
%         result_ALL.NRMSE_mean(isp,ik) = NRMSE_mean(isp,ik);
%         result_ALL.NRMSE_std(isp,ik) = NRMSE_std(isp,ik);
%         result_ALL.NRMSE_test{isp,ik} = NRMSE_test{isp,ik};
%         result_ALL.NRMSE_mean_test(isp,ik) = NRMSE_mean_test(isp,ik);
%         result_ALL.NRMSE_std_test(isp,ik) = NRMSE_std_test(isp,ik);
%         
%         save 'CIMIS_sp_3D_k_row_pri_ALL.mat' result_ALL;
        
%         clear result_ALL;
        
    end
    
%     save 'CIMIS_sparsity_3D.mat' A B C err_p_sens err_pn_test;
    %err_p_mean(ik) = sqrt(nanmean(err_p{ik}(:)));
    %err_p_mean{ik} = sqrt(nanmean(err_p{ik}));

%     DATA_PN{ik} = data_pn;
%     DATA_P{ik} = data_p;
end

% 
% for i=1:n_fold
%     
%     Nresult(i,:) = result_cv.Nerr_test{i}{1,1};
%     
% end
% 
% Nres_mean = mean(Nresult);
% 
% Nres_tot = mean(Nres_mean');

